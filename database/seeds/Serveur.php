<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Serveur extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('serveurs')->insert([
            "nameServeur"   => "local",
            "registars_id"  => 1,
            "ip"            => "192.168.10.10"
        ]);

        DB::table('serveurs')->insert([
            "nameServeur"   => "AWS 1",
            "registars_id"  => 2,
            "ip"            => "52.72.163.230"
        ]);
    }
}
