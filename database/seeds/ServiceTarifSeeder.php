<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceTarifSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("service_tarifs")->insert(["services_id" => 1, "designation" => "De 50 à 100 salariés", "montant" => 1200]);
        DB::table("service_tarifs")->insert(["services_id" => 1, "designation" => "De 101 à 200 Salariés", "montant" => 1600]);
        DB::table("service_tarifs")->insert(["services_id" => 1, "designation" => "De 201 à 500 Salariés", "montant" => 2000]);
        DB::table("service_tarifs")->insert(["services_id" => 1, "designation" => "+500 salariés par tranche de 100", "montant" => 300]);
        DB::table("service_tarifs")->insert(["services_id" => 3, "designation" => "Accès au module", "montant" => 660]);
        DB::table("service_tarifs")->insert(["services_id" => 4, "designation" => "Accès au module", "montant" => 220]);
        DB::table("service_tarifs")->insert(["services_id" => 2, "designation" => "Accès au module", "montant" => 1100]);
        DB::table("service_tarifs")->insert(["services_id" => 7, "designation" => "Accès au module", "montant" => 950]);
        DB::table("service_tarifs")->insert(["services_id" => 8, "designation" => "Accès au module", "montant" => 880]);
        DB::table("service_tarifs")->insert(["services_id" => 10, "designation" => "Accès au module", "montant" => 550]);
        DB::table("service_tarifs")->insert(["services_id" => 5, "designation" => "Accès au module", "montant" => 660]);
        DB::table("service_tarifs")->insert(["services_id" => 11, "designation" => "Maintenance effectuer", "montant" => 169]);
        DB::table("service_tarifs")->insert(["services_id" => 12, "designation" => "Tarif Bénéficiaire", "montant" => 39.90]);
        DB::table("service_tarifs")->insert(["services_id" => 12, "designation" => "Tarif Ayant Droit", "montant" => 9.90]);
    }
}
