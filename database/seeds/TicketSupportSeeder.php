<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketSupportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ticket_supports')->insert(["name" => "Conseil Technique"]);
        DB::table('ticket_supports')->insert(["name" => "Commandes & Facturations"]);
        DB::table('ticket_supports')->insert(["name" => "Déclarer un incident"]);
        DB::table('ticket_supports')->insert(["name" => "Conseil commercial & Ventes"]);
    }
}
