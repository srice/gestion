<?php

use Illuminate\Database\Seeder;

class TicketCategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('ticket_categories')->insert(["ticket_support_id" => 1, "designation" => "Utilisation de mes services"]);
        \Illuminate\Support\Facades\DB::table('ticket_categories')->insert(["ticket_support_id" => 1, "designation" => "Première mise en service"]);
        \Illuminate\Support\Facades\DB::table('ticket_categories')->insert(["ticket_support_id" => 2, "designation" => "Ma commande n'est pas finalisé"]);
        \Illuminate\Support\Facades\DB::table('ticket_categories')->insert(["ticket_support_id" => 2, "designation" => "Ma demande concerne un facture"]);
        \Illuminate\Support\Facades\DB::table('ticket_categories')->insert(["ticket_support_id" => 2, "designation" => "Information sur le renouvellement d'une commande"]);
        \Illuminate\Support\Facades\DB::table('ticket_categories')->insert(["ticket_support_id" => 3, "designation" => "Mon service est actuellement dégradé"]);
        \Illuminate\Support\Facades\DB::table('ticket_categories')->insert(["ticket_support_id" => 3, "designation" => "Mon service subi des coupures"]);
        \Illuminate\Support\Facades\DB::table('ticket_categories')->insert(["ticket_support_id" => 3, "designation" => "Mon service ne répond plus"]);
        \Illuminate\Support\Facades\DB::table('ticket_categories')->insert(["ticket_support_id" => 3, "designation" => "Mon service ne répond plus"]);
        \Illuminate\Support\Facades\DB::table('ticket_categories')->insert(["ticket_support_id" => 4, "designation" => "Commander un service supplémentaire"]);
        \Illuminate\Support\Facades\DB::table('ticket_categories')->insert(["ticket_support_id" => 4, "designation" => "Arreter un service"]);
    }
}
