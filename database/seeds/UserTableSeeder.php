<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('fr_FR');
        \App\User::create([
            "name"  => "Support SRICE",
            "email" => "support@srice.eu",
            "password"  => bcrypt("1992_Maxime"),
            "poste" => "Direction du support et du developpement"
        ]);

        if(env('APP_ENV') == 'local' || env("APP_ENV") == 'testing'){
            for($i=0; $i <= 10; $i++){
                \App\User::create([
                    "name"      => $faker->name,
                    "email"     => $faker->email,
                    "password"  => bcrypt(123456789),
                    "poste"     => $faker->jobTitle
                ]);
            }
        }
    }
}
