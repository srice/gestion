<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            "designation"   => "SRICE",
            "description"   => "Coeur du logiciel",
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "srice.png",
            "path"          => "home"
        ]);

        DB::table('modules')->insert([
            "designation"   => "Beneficiaire",
            "description"   => null,
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "beneficiaire.png",
            "path"          => "beneficiaire"
        ]);

        DB::table('modules')->insert([
            "designation"   => "Gestion des Oeuvres Sociales",
            "description"   => null,
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "gestion_asc.png",
            "path"          => "gestion_asc"
        ]);

        DB::table('modules')->insert([
            "designation"   => "Comptabilité des Oeuvres Sociales",
            "description"   => null,
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "compta_asc.png",
            "path"          => "compta_asc"
        ]);

        DB::table('modules')->insert([
            "designation"   => "Comptabilité du budget de fonctionnement",
            "description"   => null,
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "compta_fct.png",
            "path"          => "compta_fct"
        ]);

        DB::table('modules')->insert([
            "designation"   => "ANCV",
            "description"   => null,
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "ancv.png",
            "path"          => "ancv"
        ]);

        DB::table('modules')->insert([
            "designation"   => "Banker",
            "description"   => null,
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "banker.png",
            "path"          => "banker"
        ]);

        DB::table('modules')->insert([
            "designation"   => "Communication",
            "description"   => null,
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "communication.png",
            "path"          => "communication"
        ]);

        DB::table('modules')->insert([
            "designation"   => "Loca",
            "description"   => null,
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "loca.png",
            "path"          => "loca"
        ]);

        DB::table('modules')->insert([
            "designation"   => "Pret CE",
            "description"   => null,
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "pretce.png",
            "path"          => "pretce"
        ]);

        DB::table('modules')->insert([
            "designation"   => "Restogest",
            "description"   => null,
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "restogest.png",
            "path"          => "restogest"
        ]);

        DB::table('modules')->insert([
            "designation"   => "Show CE",
            "description"   => null,
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "showce.png",
            "path"          => "showce"
        ]);

        DB::table('modules')->insert([
            "designation"   => "Fidelity",
            "description"   => null,
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "fidelity.png",
            "path"          => "fidelity"
        ]);

        DB::table('modules')->insert([
            "designation"   => "Facturation Immédiate",
            "description"   => null,
            "version"       => "1.0.0",
            "release"       => 0,
            "image"         => "facturation.png",
            "path"          => "facturation"
        ]);
    }
}
