<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("services")->insert([
            "famillesId"   => 1,
            "designation"   => "Licence Logiciel",
            "description"   => null,
            "kernel"        => 1,
            "modules_id"    => 0
        ]);

        DB::table("services")->insert([
            "famillesId"   => 2,
            "designation"   => "Communication",
            "description"   => null,
            "kernel"        => 2,
            "modules_id"    => 8
        ]);

        DB::table("services")->insert([
            "famillesId"   => 2,
            "designation"   => "ANCV",
            "description"   => null,
            "kernel"        => 2,
            "modules_id"    => 6
        ]);

        DB::table("services")->insert([
            "famillesId"   => 2,
            "designation"   => "Banker",
            "description"   => null,
            "kernel"        => 2,
            "modules_id"    => 7
        ]);

        DB::table("services")->insert([
            "famillesId"   => 2,
            "designation"   => "Facturation Immédiate",
            "description"   => null,
            "kernel"        => 2,
            "modules_id"    => 14
        ]);

        DB::table("services")->insert([
            "famillesId"   => 2,
            "designation"   => "Fidelity",
            "description"   => null,
            "kernel"        => 2,
            "modules_id"    => 13
        ]);

        DB::table("services")->insert([
            "famillesId"   => 2,
            "designation"   => "Loca",
            "description"   => null,
            "kernel"        => 2,
            "modules_id"    => 9
        ]);

        DB::table("services")->insert([
            "famillesId"   => 2,
            "designation"   => "Pret CE",
            "description"   => null,
            "kernel"        => 2,
            "modules_id"    => 10
        ]);

        DB::table("services")->insert([
            "famillesId"   => 2,
            "designation"   => "Restogest",
            "description"   => null,
            "kernel"        => 2,
            "modules_id"    => 11
        ]);

        DB::table("services")->insert([
            "famillesId"   => 2,
            "designation"   => "Show CE",
            "description"   => null,
            "kernel"        => 2,
            "modules_id"    => 12
        ]);

        DB::table("services")->insert([
            "famillesId"   => 5,
            "designation"   => "Maintenance - Reprise sur compte pour erreur effectuer",
            "description"   => "Tarif de l'heure",
            "kernel"        => 0,
            "modules_id"    => 0
        ]);

        DB::table("services")->insert([
            "famillesId"   => 7,
            "designation"   => "Accès Billetterie",
            "description"   => null,
            "kernel"        => 0,
            "modules_id"    => 0
        ]);


    }
}
