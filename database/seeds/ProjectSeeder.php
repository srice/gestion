<?php

use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "SRICE-CORE", "status" => 6, "description" => "Coeur du logiciel"]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "Bénéficiaire", "status" => 6, "description" => "Permet la gestion des bénéficiaires et de leurs ayants droit"]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "Gestion des Oeuvres Sociales", "status" => 6, "description" => "Permet de gérez la billetterie de votre CSE"]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "Comptabilité des Oeuvres Sociales", "status" => 6, "description" => "Lié à la gestion des ASC, il permet d'avoir votre comptabilité à porter de main.<i>Si ce module est coupler à la Gestion des ASC, la comptabilité est automatique</i>"]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "Comptabilité du budget de fonctionnement", "status" => 6, "description" => "Depuis 2015 la comptabilité des ASC et de fonctionnement doit être séparer, ce module est similaire à la comptabilité des ASC mais doit être gérer manuellement"]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "ANCV", "status" => 2, "description" => "Si vous vendez des chèque ANCV dans votre CSE alors ce module vous permet d'en avoir la gestion dans le logiciel"]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "Banker", "status" => 5, "description" => "Il s'agit d'un agrégateur bancaire, il vous permet d'avoir une vue de vos comptes bancaires sur l'interface du logiciel."]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "Communication", "status" => 2, "description" => "Donnez la possibilité à vos bénéficiaires d'avoir une interface qui lui sont dédié."]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "Location", "status" => 1, "description" => "A la manière d'un tour opérateur, il vous permet de connectez notre billetterie et de reservez en ligne les billets pour vos bénéficiaires"]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "Pret CE", "status" => 4, "description" => "Si vous octroyer des pret à vos bénéficiaires, ce module vous permet de les gérées.<i>Si la comptabilité des ASC est actif, les pret sont enregistrer en comptabilité.</i>"]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "Restogest", "status" => 3, "description" => "Logiciel de gestion de cantine et de panier repas."]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "Show CE", "status" => 0, "description" => "Module de communication physique."]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "Fidelity", "status" => 6, "description" => "Si vous avez un système de fidélité dans votre CSE, il vous permet d'en avoir la gestion dans le logiciel."]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "Facturation immédiate", "status" => 6, "description" => "Module de facturation autre"]);
        \Illuminate\Support\Facades\DB::table('projects')->insert(["designation" => "3CX", "status" => 0, "description" => "Si vous disposez d'une infrastructure IPBX propulsé par 3CX, ce module vous permet de passer des appels directement par le logiciel."]);
    }
}
