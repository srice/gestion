<?php

use Illuminate\Database\Seeder;

class ProjectTrackActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env("APP_ENV") == 'local' || env("APP_ENV") == 'testing') {
            \Illuminate\Support\Facades\DB::table('project_track_activities')->insert([
                "track_id"  => 1,
                "note"      => "Le système est en cours de correction et sera valide pour la prochaine version"
            ]);
        }
    }
}
