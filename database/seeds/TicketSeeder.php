<?php

use Illuminate\Database\Seeder;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env('APP_ENV') == "local" || env("APP_ENV") == "test")
        {
            \Illuminate\Support\Facades\DB::table('tickets')->insert([
                "numTicket"         => "TCK".\Carbon\Carbon::now()->subDays(rand(1,30))->format('dmY').rand(100,999999),
                "univer_id"         => 1,
                "service_id" => 2,
                "support_id" => 4,
                "categorie_id"=>10,
                "sujet"             => "Commander le module ANCV",
                "state"             => 0,
                "comites_id"        => 1
            ]);

            \Illuminate\Support\Facades\DB::table('ticket_discussions')->insert([
                "ticket_id"         => "1",
                "interlocuteur"     => "CE Test",
                "message"           => "Je voudrais commander le module ANCV comment doit t-on procéder ?"
            ]);
        }
    }
}
