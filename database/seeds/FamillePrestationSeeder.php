<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FamillePrestationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('famille_prestations')->insert(["nameFamille"   => "Logiciel"]);
        DB::table('famille_prestations')->insert(["nameFamille"   => "Module"]);
        DB::table('famille_prestations')->insert(["nameFamille"   => "Formation"]);
        DB::table('famille_prestations')->insert(["nameFamille"   => "Developpement"]);
        DB::table('famille_prestations')->insert(["nameFamille"   => "Maintenance"]);
        DB::table('famille_prestations')->insert(["nameFamille"   => "Autre"]);
        DB::table('famille_prestations')->insert(["nameFamille"   => "Billetterie"]);
    }
}
