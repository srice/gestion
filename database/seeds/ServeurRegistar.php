<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServeurRegistar extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('serveur_registars')->insert([
            "name"  => "Localify",
            "logo"  => "/storage/media/img/registar/localify.png"
        ]);
        DB::table('serveur_registars')->insert([
            "name"  => "AWS EC2",
            "logo"  => "/storage/media/img/registar/ec2.png"
        ]);
    }
}
