<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContratTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contrat_types')->insert(["services_id" => 1, "name" => "Conditions général d'accès au service SRICE"]);
        DB::table('contrat_types')->insert(["services_id" => 2, "name" => "Condition général d'accès et d'utilisation des modules"]);
        DB::table('contrat_types')->insert(["services_id" => 3, "name" => "Condition général d'accès et d'utilisation des modules"]);
        DB::table('contrat_types')->insert(["services_id" => 4, "name" => "Condition général d'accès et d'utilisation des modules"]);
        DB::table('contrat_types')->insert(["services_id" => 5, "name" => "Condition général d'accès et d'utilisation des modules"]);
        DB::table('contrat_types')->insert(["services_id" => 6, "name" => "Condition général d'accès et d'utilisation des modules"]);
        DB::table('contrat_types')->insert(["services_id" => 7, "name" => "Condition général d'accès et d'utilisation des modules"]);
        DB::table('contrat_types')->insert(["services_id" => 8, "name" => "Condition général d'accès et d'utilisation des modules"]);
        DB::table('contrat_types')->insert(["services_id" => 9, "name" => "Condition général d'accès et d'utilisation des modules"]);
        DB::table('contrat_types')->insert(["services_id" => 10, "name" => "Condition général d'accès et d'utilisation des modules"]);
        DB::table('contrat_types')->insert(["services_id" => 11, "name" => "Condition général de service de maintenance"]);
        DB::table('contrat_types')->insert(["services_id" => 11, "name" => "SLA"]);
        DB::table('contrat_types')->insert(["services_id" => 12, "name" => "Condition général d'accès à la billetterie"]);
    }
}
