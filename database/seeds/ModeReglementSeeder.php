<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModeReglementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mode_reglements')->insert(["type"  => 0, "name"  => "Chèque", "limit" => null]);
        DB::table('mode_reglements')->insert(["type"  => 1, "name"  => "Carte Bancaire", "limit" => "500"]);
        DB::table('mode_reglements')->insert(["type"  => 1, "name"  => "Virement Bancaire", "limit" => "10000"]);
        DB::table('mode_reglements')->insert(["type"  => 1, "name"  => "Prélèvement Bancaire", "limit" => "10000"]);
        DB::table('mode_reglements')->insert(["type"  => 1, "name"  => "Paypal", "limit" => "1000"]);
    }
}
