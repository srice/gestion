<?php

use Illuminate\Database\Seeder;

class ProjectTrackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env("APP_ENV") == 'local' || env("APP_ENV") == 'testing') {
            \Illuminate\Support\Facades\DB::table('project_tracks')->insert([
                "project_id"        => 1,
                "version_id"        => 1,
                "reproducibility"   => 5,
                "impact"            => 3,
                "priority"          => 2,
                "subject"           => "Erreur lors de l'affichage du protocole de notification",
                "description"       => "L'affichage des notification dans le bandeau du haut pose un problème lorsque l'ont passe d'un onglet à l'autre.<br>Il affiche tous les onglets.",
                "reproduce"         => "Cliquer sur l'icone des notifications dans le bandeau du menu du haut à droite"
            ]);
        }
    }
}
