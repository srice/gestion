<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(InboxesSeeder::class);
        $this->call(ModeReglementSeeder::class);
        $this->call(FamillePrestationSeeder::class);
        $this->call(ModuleSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(ServiceTarifSeeder::class);
        $this->call(ContratTypeSeeder::class);
        $this->call(ServeurRegistar::class);
        $this->call(Serveur::class);
        $this->call(UniverSeeder::class);
        $this->call(TicketServiceSeeder::class);
        $this->call(TicketSupportSeeder::class);
        $this->call(TicketCategorieSeeder::class);
        $this->call(TicketSeeder::class);
    }
}
