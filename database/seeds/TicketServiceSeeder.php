<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ticket_services')->insert(["ticket_univer_id" => 1, "designation" => "Donnée Principal"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 1, "designation" => "Configuration"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 1, "designation" => "Bénéficiaire"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 1, "designation" => "Gestion des ASC"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 1, "designation" => "Comptabilité des ASC"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 1, "designation" => "Comptabilité du budget de fonctionnement"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 2, "designation" => "ANCV"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 2, "designation" => "Banker"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 2, "designation" => "Communication"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 2, "designation" => "LOCA"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 2, "designation" => "Pret CE"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 2, "designation" => "Restogest"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 2, "designation" => "Show CE"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 2, "designation" => "Fidelity"]);
        DB::table('ticket_services')->insert(["ticket_univer_id" => 2, "designation" => "Facturation Immédiate"]);
    }
}
