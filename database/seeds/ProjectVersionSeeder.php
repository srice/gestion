<?php

use Illuminate\Database\Seeder;

class ProjectVersionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 1, "version" => "6.2.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 2, "version" => "6.2.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 3, "version" => "6.2.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 4, "version" => "6.2.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 5, "version" => "6.2.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 6, "version" => "1.0.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 7, "version" => "1.0.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 8, "version" => "1.0.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 9, "version" => "1.0.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 10, "version" => "1.0.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 11, "version" => "1.0.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 12, "version" => "1.0.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 13, "version" => "1.0.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 14, "version" => "1.0.0"]);
        \Illuminate\Support\Facades\DB::table('project_versions')->insert(["project_id" => 15, "version" => "0.0.0"]);
    }
}
