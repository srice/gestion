<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UniverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('univers')->insert(["designation"   => "SRICE"]);
        DB::table('univers')->insert(["designation"   => "Modules"]);
    }
}
