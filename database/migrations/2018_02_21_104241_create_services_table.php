<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('famillesId');
            $table->string('designation');
            $table->text('description')->nullable();
            $table->integer('kernel')->default(0)->comment("0: Aucun|1: Espace|2: Module");
            $table->integer('modules_id')->default(0)->nullable()->comment("0: Espace à installer |id du module: module à installer");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
