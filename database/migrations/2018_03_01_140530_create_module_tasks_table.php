<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('modules_id');
            $table->string('sujet');
            $table->text('description');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->integer('state')->default(0)->comment("0: Nouveau |1: En cours |2: Terminer |3: En attente |4: Annuler");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_tasks');
    }
}
