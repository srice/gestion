<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspaceModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espace_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('espaces_id');
            $table->integer('modules_id');
            $table->integer('state')->default(0)->comment("0: Non |1: Oui");
            $table->integer('checkout')->default(0)->comment("0: Non |1: Oui");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espace_modules');
    }
}
