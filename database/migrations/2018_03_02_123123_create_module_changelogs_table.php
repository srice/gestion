<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleChangelogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_changelogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('modules_id');
            $table->string('version');
            $table->longText('description');
            $table->dateTime('dateChangelog');
            $table->integer('stateChangelog')->default(0)->comment("0: Brouillon |1: Période de Test |2: Publié");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_changelogs');
    }
}
