<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandePaiementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commande_paiements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commandes_id')->comment("Identifiant de la commande associé au paiement");
            $table->integer('modes_id')->comment("Identifiant du mode de règlement");
            $table->date('dateReglement')->comment("Date du règlement");
            $table->string('namePayeur')->comment("Nom du payeur");
            $table->string('total')->default(0)->comment("Montant du règlement");
            $table->integer('state')->default(0)->comment("0: En cours d'execution |1: Executer |2: Impossible d'executer le paiement");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commande_paiements');
    }
}
