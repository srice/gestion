<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_components', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_component_group_id');
            $table->string('name');
            $table->text("description")->nullable();
            $table->integer('state')->default(0)->comment("
            0: Opérationnel |
            1: Problème de performance |
            2: Panne partielle |
            3: Panne majeure
            ");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_components');
    }
}
