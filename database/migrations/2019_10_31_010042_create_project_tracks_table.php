<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_tracks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('version_id')->unsigned()->nullable();
            $table->integer('reproducibility')->default(0)->comment("0: Toujours |1: Quelque fois |2: Aléatoire |3: Non testé |4: Impossible |5: Sans Objet");
            $table->integer('impact')->default(0)->comment("0: Fonctionnalité |1: Simple |2: Texte |3: Apparence |4: Mineur |5: Majeur |6: Critique |7: Bloquant");
            $table->integer('priority')->default(0)->comment("0: Aucune |1: Basse |2: Normal |3: Elevée |4: Urgente |5: Immédiate");
            $table->string('subject');
            $table->text("description");
            $table->text('reproduce')->nullable();
            $table->text('note')->nullable();
            $table->integer('state')->default(0)->comment("0: Ouvert |1: Corrigé |2: Réouvert |3: Impossible à reproduire |4: Impossible à corrigé |5: Doublon |6: Pas un bogue |7: Suspendu |8: Ne sera pas corrigé");
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('version_id')->references('id')->on('project_versions')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_tracks');
    }
}
