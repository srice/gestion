<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_incidents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_component_id');
            $table->string('name');
            $table->longText('description');
            $table->integer('state')->default(0)->comment("
            0: Examen de la cause en cours |
            1: Panne identifié |
            2: Surveillance en cours |
            3: Correction en cours |
            4: Cloturer
            ");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_incidents');
    }
}
