<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactureLignesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facture_lignes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('factures_id');
            $table->integer('services_id');
            $table->text("descLigne")->nullable();
            $table->integer('qte');
            $table->string('total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facture_lignes');
    }
}
