<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspaceInstallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espace_installs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('espaces_id');
            $table->time('time')->nullable();
            $table->string("description");
            $table->integer('state')->default(0)->comment("0: Non Initialiser |1: En cours |2: réussi |3: echec");
            $table->string("descEchec")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espace_installs');
    }
}
