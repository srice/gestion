<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commandes_id')->nullable()->comment("La facture peut être affilié à une commande");
            $table->integer('comites_id');
            $table->date('date');
            $table->string('total')->default(0);
            $table->integer('state')->default(0)->comment("0: Brouillon |1: Valider |2: Non Payer |3: Partiellement Payer |4: Payer |5: Impayer");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factures');
    }
}
