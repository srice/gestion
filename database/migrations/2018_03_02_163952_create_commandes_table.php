<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comites_id');
            $table->date('date');
            $table->string('total')->default(0);
            $table->integer('state')->default(0)->comment('0: Brouillon |1: Valider |2: En cours de traitement |3:Finaliser |4: Annuler');
            $table->integer('espacesCheck')->default(0)->comment("0: Ne dépend pas d'un espace |1: Dépend d'un espace");
            $table->integer('espaces_id')->nullable()->comment("Identifiant de l'espace");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commandes');
    }
}
