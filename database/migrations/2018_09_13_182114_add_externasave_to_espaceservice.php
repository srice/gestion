<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExternasaveToEspaceservice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('espace_services', function (Blueprint $table) {
            $table->integer('external_save')->default(0)->comment('0: Sauvegarde externe désactiver |1: Sauvegarde externe actif');
            $table->integer('external_save_capacity')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('espace_services', function (Blueprint $table) {
            $table->removeColumn('external_save');
            $table->removeColumn('external_save_capacity');
        });
    }
}
