<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandeLignesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commande_lignes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commandes_id')->comment("Identifiant de la commande");
            $table->integer('services_id')->comment("Identifiant du service associé");
            $table->integer('tarifs_id');
            $table->text('description')->nullable()->comment("Description supplémentaire du service");
            $table->integer('quantite')->comment('Quantité commander sur la ligne');
            $table->string('total')->comment("Total de la ligne: Prix du service*qte");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commande_lignes');
    }
}
