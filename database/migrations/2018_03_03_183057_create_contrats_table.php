<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comites_id');
            $table->integer('types_id');
            $table->integer('commandes_id')->nullable();
            $table->date('start');
            $table->date('end');
            $table->text('description')->nullable();
            $table->integer('state')->default(0)->comment("0: Brouillon |1: Valider |2: En attente de signature client |3: Executer |4: Bientôt expirer |5: Expirer |6: Résilier");
            $table->string('signated_key')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrats');
    }
}
