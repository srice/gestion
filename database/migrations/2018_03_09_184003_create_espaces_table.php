<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espaces', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comites_id');
            $table->integer('contrats_id');
            $table->string('domaine');
            $table->string('dossier');
            $table->integer('state')->default(0)->comment("0: Hors Ligne |1: Maintenance |2: En ligne |3: Période de grace |4: Expirer");
            $table->integer('install')->default(0)->comment("0: Non Installer |1: En cours d'installation |2: Installation Effectuer |3: Erreur");
            $table->string('erreurlog')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espaces');
    }
}
