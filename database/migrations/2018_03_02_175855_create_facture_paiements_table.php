<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturePaiementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facture_paiements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('factures_id');
            $table->integer('comites_id');
            $table->integer('modes_id');
            $table->date('dateReglement');
            $table->string('namePayeur')->comment("Nom du payeur");
            $table->string('total')->default(0)->comment("Montant du règlement");
            $table->integer('state')->default(0)->comment("0: En cours d'execution |1: Executer |2: Impossible d'executer le paiement");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facture_paiements');
    }
}
