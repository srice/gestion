<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComiteContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comite_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comitesId');
            $table->string('prenom');
            $table->string('nom');
            $table->string('email');
            $table->string('position');
            $table->string('telephone');
            $table->date('created_at')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comite_contacts');
    }
}
