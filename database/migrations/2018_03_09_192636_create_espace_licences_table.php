<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspaceLicencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espace_licences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('espaces_id');
            $table->string('numLicence');
            $table->integer('module')->default(0)->comment("0: Non |1: Oui");
            $table->integer('modules_id')->nullable();
            $table->date('start');
            $table->date('end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espace_licences');
    }
}
