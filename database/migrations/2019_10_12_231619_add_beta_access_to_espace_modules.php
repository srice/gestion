<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBetaAccessToEspaceModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('espace_modules', function (Blueprint $table) {
            $table->integer('beta_access')->default(0);
            $table->string('beta_access_key')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('espace_modules', function (Blueprint $table) {
            $table->removeColumn('beta_access');
            $table->removeColumn('beta_access_key');
        });
    }
}
