function showAlert()
{
    let div = $("#timeline-alert")

    $.ajax({
        url: '/showAlert',
        success: function (data) {
            div.html(data)
        }
    })
}

function showEvent()
{
    let div = $("#timeline-event")

    $.ajax({
        url: '/showEvent',
        success: function (data) {
            div.html(data)
        }
    })
}

function showLogs()
{
    let div = $("#timeline-logs")

    $.ajax({
        url: '/showLogs',
        success: function (data) {
            div.html(data)
        }
    })
}

function countNotif()
{
    let div = $("#countNotif")

    $.ajax({
        url: '/countNotif',
        success: function (data) {
            div.html(data)
        }
    })
}

function dotNotif()
{
    let div = $("#dot-notif")

    $.ajax({
        url: '/dotNotif',
        success: function (data) {
            div.html(data)
        }
    })
}

let notificationWrapper         = $(".m-topbar__notifications")
let notificationToggle          = $("#m_topbar_notification_icon")
let notificationCountElem       =  notificationToggle.find('i[data-count]')
let notificationCount           = parseInt(notificationCountElem.data('count'))
