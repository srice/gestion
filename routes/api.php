<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/connect', 'Api\ConnectController@connect');

Route::group(["prefix" => "comite", "namespace" => "Api\Comite"], function (){
    Route::get('/list', 'ComiteController@list');
    Route::get('/{comite_id}', 'ComiteController@get');
});

Route::group(["prefix" => "prestation", "namespace" => "Api\Prestation"], function (){
    Route::group(["prefix" => "famille", "namespace" => "Famille"], function (){
        Route::get('/', 'FamilleController@list');
        Route::get('{famille_id}', 'FamilleController@famille');
    });

    Route::group(["prefix" => "Service", "namespace" => "Service"], function (){
        Route::get('/', 'ServiceController@list');
        Route::get('{service_id}', 'ServiceController@service');
    });

    Route::group(["prefix" => "module", "namespace" => "Module"], function (){
        Route::get('/', 'ModuleController@listModules');
        Route::get('{modules_id}', 'ModuleController@getModule');
        Route::get('{modules_id}/countTasks', 'ModuleController@countTasks');
        Route::get('{modules_id}/countChangelogs', 'ModuleController@countChangelogs');

        Route::group(["prefix" => "{modules_id}/task", "namespace" => "Task"], function () {
            Route::get('/', 'TaskController@list');
            Route::get('/count', 'TaskController@count');
            Route::get('{task_id}', 'TaskController@task');
        });

        Route::group(["prefix" => "{modules_id}/changelog", "namespace" => "Changelog"], function () {
            Route::get('/', 'ChangelogController@list');
            Route::get('/count', 'ChangelogController@count');
            Route::get('{changelog_id}', 'ChangelogController@changelog');
        });
    });
});

Route::group(["prefix" => "facturation", "namespace" => "Api\Facturation"], function () {
    Route::group(["prefix" => "commande", "namespace" => "Commande"], function () {

    });

    Route::group(["prefix" => "facture", "namespace" => "Facture"], function () {

    });

    Route::group(["prefix" => "contrat", "namespace" => "Contrat"], function () {

    });
});

Route::group(["prefix" => "espace", "namespace" => "Api\Espace"], function (){
    Route::get('{comite_id}', 'EspaceController@getByComite');

    Route::post('{espace_id}/beta', 'EspaceController@storeBeta');
});

Route::group(["prefix" => "infrastructure", "namespace" => "Api\Infrastructure"], function (){
    Route::group(["prefix" => "server", "namespace" => "Server"], function () {
        Route::get('/check/server1', 'ServerController@checkServerOne');
        Route::get('/check/server2', 'ServerController@checkServerTwo');
        Route::get('/check/serverDns', 'ServerController@checkServerDns');
        Route::get('/check/serverBack1', 'ServerController@checkServerBackupOne');
        Route::get('/check/serverBack2', 'ServerController@checkServerBackupTwo');
        Route::get('/check/serverBanker', 'ServerController@checkServerBanker');
        Route::get('/check/serverBilletterie', 'ServerController@checkServerBilletterie');
        Route::get('/check/phone', 'ServerController@checkPhone');
        Route::get('/check/mail', 'ServerController@checkMail');
        Route::get('/check/chat', 'ServerController@checkChat');

        Route::get('/check/full', 'ServerController@checkFull');
    });
});

Route::group(["prefix" => "web", "namespace" => "Api\Web"], function () {
    Route::group(["prefix" => "blog", "namespace" => "Blog"], function () {
        Route::get('/', 'BlogController@list');
        Route::get('{blog_id}', 'BlogController@blog');

        Route::group(["prefix" => "{blog_id}/comment", "namespace" => "Comment"], function (){
            Route::get('/', 'CommentController@list');
        });
    });
});

Route::group(["prefix" => "notification", "namespace" => "Api\Notification"], function () {

});

Route::group(["prefix" => "sociales", "namespace" => "Api\Sociales"], function () {
    Route::group(["prefix" => "facebook", "namespace" => "Facebook"], function () {

    });

    Route::group(["prefix" => "twitter", "namespace" => "Twitter"], function () {

    });
});

Route::group(["prefix" => "support", "namespace" => "Api\Support"], function (){
    Route::group(['prefix' => "ticket", "namespace" => "Ticket"], function (){
        Route::get('/list/{comites_id}', 'TicketController@listForComite');

        Route::group(["prefix" => "univers"], function (){
            Route::get('/list', 'TicketUniversController@listUnivers');
            Route::post('/', 'TicketUniversController@search');
        });
    });

    Route::group(["prefix" => "tracker", "namespace" => "Tracker"], function () {
        Route::group(["prefix" => "project", "namespace" => "Project"], function (){

        });
    });

    Route::group(["prefix" => "status", "namespace" => "Status"], function (){
        Route::group(["prefix" => "group", "namespace" => "Group"], function (){
            Route::get('/list', 'GroupController@list');
            Route::get('/{group_id}', 'GroupController@get');

            Route::group(["prefix" => "{group_id}/component", "namespace" => "Component"], function (){
                Route::get('/list', 'ComponentController@list');
                Route::get('/countGreen', 'ComponentController@countGreen');
                Route::get('/countOrange', 'ComponentController@countOrange');
                Route::get('/countRed', 'ComponentController@countRed');

                Route::get('/{component_id}', 'ComponentController@get');

                Route::group(["prefix" => "{component_id}/incident", "namespace" => "Incident"], function (){
                    Route::get('/list', 'IncidentController@list');
                    Route::get('/list/day', 'IncidentController@listDay');
                    Route::get('/{incident_id}', 'IncidentController@get');
                });
            });
        });

        Route::group(["prefix" => "maintenance", "namespace" => "Maintenance"], function (){
            Route::get('/list', 'MaintenanceController@list');
            Route::get('/{maintenance_id}', 'MaintenanceController@get');
        });

        Route::group(["prefix" => "component", "namespace" => "Component"], function () {
            Route::get('{component_id}', 'ComponentController@get');
        });

        Route::group(["prefix" => "incident", "namespace" => "Incident"], function (){
            Route::get('list', 'IncidentController@list');
            Route::get('days', 'IncidentController@days');
            Route::get('{incident_id}', 'IncidentController@get');
        });
    });
});
