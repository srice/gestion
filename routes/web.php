<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(["middleware" => "auth"], function (){
    Route::get('/', "HomeController@index")->name('home');
    Route::get('/user/profil', ["as" => "User.profil", "uses" => "UserController@profil"]);
    Route::put('/user/profil', ["as" => "User.update", "uses" => "UserController@update"]);

    Route::group(["prefix" => "comite", "namespace" => "Comite"], function (){
        Route::get('/', ["as" => "Comite.index", "uses" => "ComiteController@index"]);
        Route::get('create', ["as" => "Comite.create", "uses" => "ComiteController@create"]);
        Route::post('create', ["as" => "Comite.store", "uses" => "ComiteController@store"]);
        Route::get('{comites_id}', ["as" => "Comite.show", "uses" => "ComiteController@show"]);
        Route::get('{comites_id}/edit', ["as" => "Comite.edit", "uses" => "ComiteController@edit"]);
        Route::put('{comites_id}/edit', ["as" => "Comite.update", "uses" => "ComiteController@update"]);
        Route::delete('{comites_id}', ["as" => "Comite.delete", "uses" => "ComiteController@delete"]);
    });

    Route::group(["prefix" => "prestation", "namespace" => "Prestation"], function (){
        Route::get('/', ["as" => "Prestation.index", "uses" => "PrestationController@index"]);

        Route::group(["prefix" => "famille", "namespace" => "Famille"], function (){
            Route::get('/', ["as" => "Famille.index", 'uses' => "FamilleController@index"]);
        });

        Route::group(["prefix" => "produit", "namespace" => "Produit"], function (){
            Route::get('/', ["as" => "Produit.index", "uses" => "ProduitController@index"]);
        });

        Route::group(["prefix" => "module", "namespace" => "Module"], function (){
            Route::get('/', ["as" => "Module.index", "namespace" => "ModuleController@index"]);
        });
    });

    Route::group(["prefix" => "facturation", "namespace" => "Facturation"], function (){
        Route::get('/', ["as" => "Facturation.index", "uses" => "FacturationController@index"]);


    });

});

Auth::routes();
Route::get('logout', "Auth\LoginController@logout")->name('logout');

Route::get('showAlert', "NotificationController@showAlert");
Route::get('showEvent', "NotificationController@showEvent");
Route::get('showLogs', "NotificationController@showLogs");
Route::get('countNotif', "NotificationController@countNotif");
Route::get('dotNotif', "NotificationController@dotNotif");
Route::get('search', "SearchController@search");

Route::get('code', "TestController@code");
