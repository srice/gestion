<?php

namespace App\Http\Controllers\Comite;

use App\Repository\Comite\ComiteContactRepository;
use App\Repository\Comite\ComiteRepository;
use App\Repository\Core\NotificationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class ComiteController extends Controller
{
    /**
     * @var ComiteRepository
     */
    private $comiteRepository;
    private $sector;
    /**
     * @var ComiteContactRepository
     */
    private $comiteContactRepository;

    /**
     * ComiteController constructor.
     * @param ComiteRepository $comiteRepository
     * @param ComiteContactRepository $comiteContactRepository
     */
    public function __construct(ComiteRepository $comiteRepository, ComiteContactRepository $comiteContactRepository)
    {
        $this->comiteRepository = $comiteRepository;
        $this->sector = 'comite';
        $this->comiteContactRepository = $comiteContactRepository;
    }

    public function index()
    {
        $config = (object)[
            "sector"    => $this->sector
        ];
        return view('Comite.index', [
            "config"    => $config,
            "comites"   => $this->comiteRepository->list()
        ]);
    }

    public function create()
    {
        $config = (object)[
            "sector"    => $this->sector
        ];
        return view('Comite.create', [
            "config"    => $config
        ]);
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $comite = $this->comiteRepository->create(
            $request->get('name'),
            $request->get('adresse'),
            $request->get('cp'),
            $request->get('ville'),
            $request->get('tel'),
            $request->get('email')
        );

        $contact = $this->comiteContactRepository->create(
            $comite->id,
            $request->get('prenom'),
            $request->get('nom'),
            $request->get('email_contact'),
            $request->get('position'),
            $request->get('tel_contact')
        );

        NotificationRepository::createStaticLog('0', 'COMITE', "Le Comite ".$comite->name." à été créer");
        return null;
    }

    public function show($id)
    {
        //dd($this->comiteRepository->get($id));
        $config = (object)[
            "sector"    => $this->sector
        ];
        return view("Comite.show", [
            "comite"    => $this->comiteRepository->get($id),
            "config"    => $config
        ]);
    }
}
