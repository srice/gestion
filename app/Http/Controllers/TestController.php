<?php

namespace App\Http\Controllers;

use App\Repository\Core\NotificationRepository;
use App\Repository\Support\Tracker\Project\ProjectRepository;
use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * TestController constructor.
     * @param ProjectRepository $projectRepository
     */
    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    public function code()
    {
        dd($this->projectRepository->list());
    }
}
