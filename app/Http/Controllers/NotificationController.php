<?php

namespace App\Http\Controllers;

use App\Repository\Core\NotificationRepository;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * @var NotificationRepository
     */
    private $notificationRepository;

    /**
     * NotificationController constructor.
     * @param NotificationRepository $notificationRepository
     */
    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    public function showAlert()
    {
        try{
            $countAlert = $this->notificationRepository->countAlert();
        }catch (\Exception $ex){
            Bugsnag::notifyException($ex);
        }
        try{
            $notifications = $this->notificationRepository->getAlert();
        }catch (\Exception $ex)
        {
            Bugsnag::notifyException($ex);
        }
        ob_start();
        ?>
        <?php if($countAlert != 0): ?>
        <?php foreach ($notifications as $notification): ?>
            <div class="m-list-timeline__item">
                <span class="m-list-timeline__badge <?= stateNotification($notification->stateNotify); ?>"></span>
                <span class="m-list-timeline__text"><strong><?= $notification->localNotify; ?>:</strong> <?= $notification->data; ?></span>
                <span class="m-list-timeline__time"><?= $notification->created_at->diffForHumans(); ?></span>
            </div>
        <?php endforeach; ?>
        <?php else: ?>
        <div class="m-stack__item m-stack__item--center m-stack__item--middle">
            <span class="">Aucune notification</span>
        </div>
        <?php endif; ?>
        <?php
        $content = ob_get_clean();
        return response()->json([$content]);
    }

    public function showEvent()
    {
        $countAlert = $this->notificationRepository->countEvent();
        $notifications = $this->notificationRepository->getEvent();
        ob_start();
        ?>
        <?php if($countAlert != 0): ?>
        <?php foreach ($notifications as $notification): ?>
            <div class="m-list-timeline__item">
                <span class="m-list-timeline__badge <?= stateNotification($notification->stateNotify); ?>"></span>
                <span class="m-list-timeline__text"><strong><?= $notification->localNotify; ?>:</strong> <?= $notification->data; ?></span>
                <span class="m-list-timeline__time"><?= $notification->created_at->diffForHumans(); ?></span>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="m-stack__item m-stack__item--center m-stack__item--middle">
            <span class="">Aucune notification</span>
        </div>
    <?php endif; ?>
        <?php
        $content = ob_get_clean();
        return response()->json([$content]);
    }

    public function showLogs()
    {
        $countAlert = $this->notificationRepository->countLogs();
        $notifications = $this->notificationRepository->getLogs();
        ob_start();
        ?>
        <?php if($countAlert != 0): ?>
        <?php foreach ($notifications as $notification): ?>
            <div class="m-list-timeline__item">
                <span class="m-list-timeline__badge <?= stateNotification($notification->stateNotify); ?>"></span>
                <span class="m-list-timeline__text"><strong><?= $notification->localNotify; ?>:</strong> <?= $notification->data; ?></span>
                <span class="m-list-timeline__time"><?= $notification->created_at->diffForHumans(); ?></span>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="m-stack__item m-stack__item--center m-stack__item--middle">
            <span class="">Aucune notification</span>
        </div>
    <?php endif; ?>
        <?php
        $content = ob_get_clean();
        return response()->json([$content]);
    }

    public function countNotif()
    {
        $countNotif = $this->notificationRepository->countNotif();
        ob_start();
        ?>
        <?php if($countNotif == 0): ?>
            Aucune Nouvelle
        <?php else: ?>
            <?= $countNotif; ?> <?= str_plural('Nouvelle', $countNotif); ?>
        <?php endif; ?>
        <?php
        $content = ob_get_clean();
        return response()->json([$content]);
    }

    public function dotNotif()
    {
        $countNotif = $this->notificationRepository->countNotif();
        ob_start();
        ?>
        <?php if($countNotif != 0): ?>
        <span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>
        <?php endif; ?>
        <?php
        $content = ob_get_clean();
        return response()->json([$content]);
    }
}
