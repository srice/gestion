<?php

namespace App\Http\Controllers;

use App\Notifications\Automate\CleanNotification;
use App\Repository\Core\NotificationRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class AutomateController extends Controller
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var NotificationRepository
     */
    private $notificationRepository;

    /**
     * AutomateController constructor.
     * @param User $user
     * @param NotificationRepository $notificationRepository
     */
    public function __construct(User $user, NotificationRepository $notificationRepository)
    {

        $this->user = $user;
        $this->notificationRepository = $notificationRepository;
    }

    public function cleanNotification()
    {
        $users = $this->user->newQuery()->get();
        $clean = $this->notificationRepository->cleanNotification();
        if ($clean > 0)
        {
            foreach ($users as $user){
                $user->notify(new CleanNotification());
            }

        }
    }
}
