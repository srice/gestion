<?php

namespace App\Http\Controllers\Api\Prestation\Module\Task;

use App\Http\Controllers\Api\BaseController;
use App\Repository\Prestation\Module\ModuleTaskRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends BaseController
{
    /**
     * @var ModuleTaskRepository
     */
    private $moduleTaskRepository;

    /**
     * TaskController constructor.
     * @param ModuleTaskRepository $moduleTaskRepository
     */
    public function __construct(ModuleTaskRepository $moduleTaskRepository)
    {
        $this->moduleTaskRepository = $moduleTaskRepository;
    }

    public function count($modules_id)
    {
        try {
            $count = $this->moduleTaskRepository->countForModule($modules_id);
            return $this->sendResponse($count, 'Nombre de tache');
        } catch (\Exception $exception) {
            return $this->sendError("Erreur de récupération du nombre de tache", [
                "exception" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }

    public function list($modules_id)
    {
        try {
            $list = $this->moduleTaskRepository->list($modules_id);
            return $this->sendResponse($list->toArray(), "Liste des taches d'un module");
        } catch (\Exception $exception) {
            return $this->sendError("Erreur de récupération de la liste des tache", [
                "exception" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }

    public function task($modules_id, $task_id)
    {
        try {
            $task = $this->moduleTaskRepository->get($task_id);
            return $this->sendResponse($task->toArray(), "Information sur une tache particulière");
        } catch (\Exception $exception) {
            return $this->sendError("Erreur de récupération des information d'une tache particulière", [
                "exception" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }


}
