<?php

namespace App\Http\Controllers\Api\Prestation\Module\Changelog;

use App\Http\Controllers\Api\BaseController;
use App\Repository\Prestation\Module\ModuleChangelogRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChangelogController extends BaseController
{
    /**
     * @var ModuleChangelogRepository
     */
    private $moduleChangelogRepository;

    /**
     * ChangelogController constructor.
     * @param ModuleChangelogRepository $moduleChangelogRepository
     */
    public function __construct(ModuleChangelogRepository $moduleChangelogRepository)
    {
        $this->moduleChangelogRepository = $moduleChangelogRepository;
    }

    public function list($modules_id)
    {
        try {
            $list = $this->moduleChangelogRepository->list($modules_id);
            return $this->sendResponse($list->toArray(), "Liste des notes d'un module particulier");
        }catch (\Exception $exception) {
            return $this->sendError("Erreur de récupération de la liste des notes", [
                "exception" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }

    public function count($modules_id)
    {
        try {
            $count = $this->moduleChangelogRepository->countForModule($modules_id);
            return $this->sendResponse($count, "Nombre de mise à jour");
        } catch (\Exception $exception) {
            return $this->sendError("Erreur de récupération du nombre de mise à jour", [
                "exception" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }

    public function changelog($modules_id, $changelog_id)
    {
        try {
            $changelog = $this->moduleChangelogRepository->get($changelog_id);
            return $this->sendResponse($changelog->toArray(), "Information sur un changelog particulier");
        } catch (\Exception $exception) {
            return $this->sendError("Erreur de récupération de l'information de changelog", [
                "exception" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }
}
