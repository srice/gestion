<?php

namespace App\Http\Controllers\Api\Prestation\Module;

use App\Http\Controllers\Api\BaseController;
use App\Repository\Prestation\Module\ModuleChangelogRepository;
use App\Repository\Prestation\Module\ModuleRepository;
use App\Repository\Prestation\Module\ModuleTaskRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModuleController extends BaseController
{
    /**
     * @var ModuleRepository
     */
    private $moduleRepository;
    /**
     * @var ModuleTaskRepository
     */
    private $moduleTaskRepository;
    /**
     * @var ModuleChangelogRepository
     */
    private $moduleChangelogRepository;

    /**
     * ModuleController constructor.
     * @param ModuleRepository $moduleRepository
     * @param ModuleTaskRepository $moduleTaskRepository
     * @param ModuleChangelogRepository $moduleChangelogRepository
     */
    public function __construct(ModuleRepository $moduleRepository, ModuleTaskRepository $moduleTaskRepository, ModuleChangelogRepository $moduleChangelogRepository)
    {
        $this->moduleRepository = $moduleRepository;
        $this->moduleTaskRepository = $moduleTaskRepository;
        $this->moduleChangelogRepository = $moduleChangelogRepository;
    }

    public function listModules()
    {
        $module = $this->moduleRepository->list();
        return $this->sendResponse($module->toArray(), "Liste des modules");
    }

    public function getModule($modules_id)
    {
        $module = $this->moduleRepository->get($modules_id);
        $array = [
            "module"    => $module->toArray(),
            "tasks"     => $module->tasks->toArray(),
            "changelogs"=> $module->changelogs->toArray()
        ];

        return $this->sendResponse($array, 'Information sur un module');
    }
}
