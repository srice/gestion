<?php

namespace App\Http\Controllers\Api\Comite;

use App\Http\Controllers\Api\BaseController;
use App\Repository\Comite\ComiteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComiteController extends BaseController
{
    /**
     * @var ComiteRepository
     */
    private $comiteRepository;

    /**
     * ComiteController constructor.
     * @param ComiteRepository $comiteRepository
     */
    public function __construct(ComiteRepository $comiteRepository)
    {

        $this->comiteRepository = $comiteRepository;
    }

    public function list()
    {
        $comites = $this->comiteRepository->list();
        return $this->sendResponse($comites->toArray(), "Liste des comités");
    }

    public function get($comite_id)
    {
        $comite = $this->comiteRepository->getForApi($comite_id);
        return $this->sendResponse($comite->toArray(), "Comité Importer avec succès");
    }
}
