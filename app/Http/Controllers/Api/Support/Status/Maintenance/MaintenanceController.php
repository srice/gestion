<?php

namespace App\Http\Controllers\Api\Support\Status\Maintenance;

use App\Http\Controllers\Api\BaseController;
use App\Repository\Support\Status\StatusMaintenanceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MaintenanceController extends BaseController
{
    /**
     * @var StatusMaintenanceRepository
     */
    private $maintenanceRepository;

    /**
     * MaintenanceController constructor.
     * @param StatusMaintenanceRepository $maintenanceRepository
     */
    public function __construct(StatusMaintenanceRepository $maintenanceRepository)
    {
        $this->maintenanceRepository = $maintenanceRepository;
    }

    public function list()
    {
        return $this->sendResponse($this->maintenanceRepository->list()->toArray(), 'Liste des Maintenance');
    }

    public function get($maintenance_id)
    {
        return $this->sendResponse($this->maintenanceRepository->get($maintenance_id)->toArray(), 'Voir les infos d\'une Maintenance');
    }
}
