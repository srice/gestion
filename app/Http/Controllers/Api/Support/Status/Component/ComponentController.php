<?php

namespace App\Http\Controllers\Api\Support\Status\Component;

use App\Http\Controllers\Api\BaseController;
use App\Repository\Support\Status\StatusComponentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComponentController extends BaseController
{
    /**
     * @var StatusComponentRepository
     */
    private $statusComponentRepository;

    /**
     * ComponentController constructor.
     * @param StatusComponentRepository $statusComponentRepository
     */
    public function __construct(StatusComponentRepository $statusComponentRepository)
    {
        $this->statusComponentRepository = $statusComponentRepository;
    }

    public function get($component_id)
    {
        return $this->sendResponse($this->statusComponentRepository->get($component_id)->toArray(), "Information sur un component");
    }
}
