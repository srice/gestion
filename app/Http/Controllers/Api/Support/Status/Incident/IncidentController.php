<?php

namespace App\Http\Controllers\Api\Support\Status\Incident;

use App\Http\Controllers\Api\BaseController;
use App\Repository\Support\Status\StatusIncidentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IncidentController extends BaseController
{
    /**
     * @var StatusIncidentRepository
     */
    private $statusIncidentRepository;

    /**
     * IncidentController constructor.
     * @param StatusIncidentRepository $statusIncidentRepository
     */
    public function __construct(StatusIncidentRepository $statusIncidentRepository)
    {
        $this->statusIncidentRepository = $statusIncidentRepository;
    }

    public function list()
    {
        return $this->sendResponse($this->statusIncidentRepository->list()->toArray(), "Liste des Incident");
    }

    public function days()
    {
        return $this->sendResponse($this->statusIncidentRepository->days()->toArray(), "Liste des incident du jour");
    }

    public function get($incident_id)
    {
        return $this->sendResponse($this->statusIncidentRepository->get($incident_id)->toArray(), "Information sur un incident");
    }
}
