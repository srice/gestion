<?php

namespace App\Http\Controllers\Api\Support\Status\Group\Component\Incident;

use App\Http\Controllers\Api\BaseController;
use App\Repository\Support\Status\StatusIncidentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IncidentController extends BaseController
{
    /**
     * @var StatusIncidentRepository
     */
    private $incidentRepository;

    /**
     * IncidentController constructor.
     * @param StatusIncidentRepository $incidentRepository
     */
    public function __construct(StatusIncidentRepository $incidentRepository)
    {
        $this->incidentRepository = $incidentRepository;
    }

    public function list($group_id, $component_id)
    {
        return $this->sendResponse($this->incidentRepository->list($group_id, $component_id)->toArray(), "Liste des incidents");
    }

    public function get($group_id, $component_id, $incident_id)
    {
        return $this->sendResponse($this->incidentRepository->get($group_id, $component_id, $incident_id)->toArray(), "Voir un incident");
    }
}
