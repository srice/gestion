<?php

namespace App\Http\Controllers\Api\Support\Status\Group\Component;

use App\Http\Controllers\Api\BaseController;
use App\Repository\Support\Status\StatusComponentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComponentController extends BaseController
{
    /**
     * @var StatusComponentRepository
     */
    private $componentRepository;

    /**
     * ComponentController constructor.
     * @param StatusComponentRepository $componentRepository
     */
    public function __construct(StatusComponentRepository $componentRepository)
    {
        $this->componentRepository = $componentRepository;
    }


    public function list($group_id)
    {
        return $this->sendResponse($this->componentRepository->list($group_id)->toArray(), "Liste des composants");
    }

    public function get($group_id, $component_id)
    {
        return $this->sendResponse($this->componentRepository->get($group_id, $component_id)->toArray(), "Voir un composant");
    }

    public function countGreen($group_id)
    {
        $count = $this->componentRepository->countOperationnel($group_id);

        return $this->sendResponse($count, "Nombre de composant opérationnel");
    }

    public function countOrange($group_id)
    {
        $count = $this->componentRepository->countOrange($group_id);

        return $this->sendResponse($count, "Nombre de composant Warning");
    }

    public function countRed($group_id)
    {
        $count = $this->componentRepository->countRed($group_id);

        return $this->sendResponse($count, "Nombre de composant Erreur");
    }
}
