<?php

namespace App\Http\Controllers\Api\Support\Status\Group;

use App\Http\Controllers\Api\BaseController;
use App\Repository\Support\Status\StatusComponentGroupRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupController extends BaseController
{
    /**
     * @var StatusComponentGroupRepository
     */
    private $componentGroupRepository;

    /**
     * GroupController constructor.
     * @param StatusComponentGroupRepository $componentGroupRepository
     */
    public function __construct(StatusComponentGroupRepository $componentGroupRepository)
    {
        $this->componentGroupRepository = $componentGroupRepository;
    }

    public function list()
    {
        return $this->sendResponse($this->componentGroupRepository->list()->toArray(), "Liste des Groupes");
    }

    public function get($group_id)
    {
        return $this->sendResponse($this->componentGroupRepository->get($group_id), "Voir un groupe");
    }
}
