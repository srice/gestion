<?php

namespace App\Http\Controllers\Api\Support\Ticket;

use App\Http\Controllers\Api\BaseController;
use App\Repository\Support\Ticket\UniverRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TicketUniversController extends BaseController
{
    /**
     * @var UniverRepository
     */
    private $univerRepository;

    /**
     * TicketUniversController constructor.
     * @param UniverRepository $univerRepository
     */
    public function __construct(UniverRepository $univerRepository)
    {
        $this->univerRepository = $univerRepository;
    }

    public function listUnivers()
    {
        try {
            $univers = $this->univerRepository->all();
            $count = $this->univerRepository->countAll();

            return $this->sendResponse([$univers->toArray(), "total_count" => $count], "Liste des Univers du support");
        } catch (\Exception $exception) {
            return $this->sendError("Erreur", [
                "message"   => $exception->getMessage(),
                "trace"     => $exception->getTrace(),
                "code"      => $exception->getCode()
            ], 221);
        }
    }

    public function search(Request $request){
        try {
            $univers = $this->univerRepository->searchByTerm($request->q);
            $count = $this->univerRepository->countByTerm($request->q);

            return $this->sendResponse([
                "univers" => $univers->toArray(),
                "total_count"   => $count
            ], "Liste des univers par terme");
        } catch(\Exception $exception) {
            return $this->sendError("Erreur", [
                "message"   => $exception->getMessage(),
                "trace"     => $exception->getTrace(),
                "code"      => $exception->getCode()
            ], 221);
        }

    }
}
