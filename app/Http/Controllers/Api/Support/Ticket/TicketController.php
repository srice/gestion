<?php

namespace App\Http\Controllers\Api\Support\Ticket;

use App\Http\Controllers\Api\BaseController;
use App\Repository\Support\Ticket\TicketRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TicketController extends BaseController
{
    /**
     * @var TicketRepository
     */
    private $ticketRepository;

    /**
     * TicketController constructor.
     * @param TicketRepository $ticketRepository
     */
    public function __construct(TicketRepository $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }

    public function listForComite($comites_id)
    {
        $tickets = $this->ticketRepository->listForComite($comites_id);
    }
}
