<?php

namespace App\Http\Controllers\API\Infrastructure\Server;

use App\Http\Controllers\Api\BaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServerController extends BaseController
{
    public function __construct()
    {
    }

    public function checkServerOne()
    {
        try {
            $ip = fsockopen('server1.srice.eu', '80');
            return $this->sendResponse(['connect' => true], "Accès OK");
        }catch (\Exception $exception) {
            return $this->sendError(false, [
                "code"  => $exception->getCode(),
                "message" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }

    public function checkServerTwo()
    {
        try {
            $ip = fsockopen('server2.srice.eu', '80');
            return $this->sendResponse(['connect' => true], "Accès OK");
        }catch (\Exception $exception) {
            return $this->sendError(false, [
                "code"  => $exception->getCode(),
                "message" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }

    public function checkServerDns()
    {
        try {
            $ip = fsockopen('ns1126s.ui-dns.com', '53');
            return $this->sendResponse(['connect' => true], "Accès OK");
        }catch (\Exception $exception) {
            return $this->sendError(false, [
                "code"  => $exception->getCode(),
                "message" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }

    public function checkServerBackupOne()
    {
        try {
            $ip = fsockopen('backup1.srice.eu', '80');
            return $this->sendResponse(['connect' => true], "Accès OK");
        }catch (\Exception $exception) {
            return $this->sendError(false, [
                "code"  => $exception->getCode(),
                "message" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }

    public function checkServerBackupTwo()
    {
        try {
            $ip = fsockopen('backup2.srice.eu', '80');
            return $this->sendResponse(['connect' => true], "Accès OK");
        }catch (\Exception $exception) {
            return $this->sendError(false, [
                "code"  => $exception->getCode(),
                "message" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }

    public function checkServerBanker()
    {
        try {
            $ip = fsockopen('sync.bankin.com/v2/', '80');
            return $this->sendResponse(['connect' => true], "Accès OK");
        }catch (\Exception $exception) {
            return $this->sendError(false, [
                "code"  => $exception->getCode(),
                "message" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }

    public function checkServerBilletterie()
    {
        try {
            $ip = fsockopen('resabilletcse.com', '80');
            return $this->sendResponse(['connect' => true], "Accès OK");
        }catch (\Exception $exception) {
            return $this->sendError(false, [
                "code"  => $exception->getCode(),
                "message" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }

    public function checkPhone()
    {
        try {
            return $this->sendResponse(true, "Accès OK");
        }catch (\Exception $exception) {
            return $this->sendError(false, [
                "code"  => $exception->getCode(),
                "message" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ], 500);
        }
    }

    public function checkMail()
    {
        try {
            $ip = fsockopen('srice.eu', '587', $errno, $errstr);
            return $this->sendResponse(['connect' => true], "Accès OK");
        }catch (\Exception $exception) {
            return $this->sendError(false, [
                "code"  => $exception->getCode(),
                "message" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }

    public function checkChat()
    {
        try {
            $ip = fsockopen('chat.srice.eu', '80');
            return $this->sendResponse(['connect' => true], "Accès OK");
        }catch (\Exception $exception) {
            return $this->sendError(false, [
                "code"  => $exception->getCode(),
                "message" => $exception->getMessage(),
                "trace" => $exception->getTrace()
            ]);
        }
    }

    public function checkFull()
    {
        $server1 = $this->checkServerOne();
        $server2 = $this->checkServerTwo();
        $serverDns = $this->checkServerDns();
        $backup1 = $this->checkServerBackupOne();
        $backup2 = $this->checkServerBackupTwo();
        $banker = $this->checkServerBanker();
        $billet = $this->checkServerBilletterie();
        $phone = $this->checkPhone();
        $mail = $this->checkMail();
        $chat = $this->checkChat();

        if($server1['data']['connect'] == true &&
            $server2['data']['connect'] == true &&
            $serverDns['data']['connect'] == true &&
            $backup1['data']['connect'] == true &&
            $backup2['data']['connect'] == true &&
            $banker['data']['connect'] == true &&
            $billet['data']['connect'] == true &&
            $phone['data']['connect'] == true &&
            $mail['data']['connect'] == true &&
            $chat['data']['connect'] == true
        ) {
            return $this->sendResponse(true, "OK");
        }else{
            return $this->sendResponse(false, "Nop");
        }
    }
}
