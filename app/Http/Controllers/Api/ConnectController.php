<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConnectController extends BaseController
{
    public function connect()
    {
        return $this->sendResponse(["code" => 200], "Api Connecter");
    }
}
