<?php

namespace App\Http\Controllers\Api\Espace;

use App\Http\Controllers\Api\BaseController;
use App\Repository\Espace\EspaceModuleRepository;
use App\Repository\Espace\EspaceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EspaceController extends BaseController
{
    /**
     * @var EspaceRepository
     */
    private $espaceRepository;
    /**
     * @var EspaceModuleRepository
     */
    private $moduleRepository;

    /**
     * EspaceController constructor.
     * @param EspaceRepository $espaceRepository
     * @param EspaceModuleRepository $moduleRepository
     */
    public function __construct(EspaceRepository $espaceRepository, EspaceModuleRepository $moduleRepository)
    {
        $this->espaceRepository = $espaceRepository;
        $this->moduleRepository = $moduleRepository;
    }

    public function getByComite($comite_id)
    {
        $espace = $this->espaceRepository->getByComite($comite_id);

        return $this->sendResponse($espace->toArray(), 'Informations de l\'espace par le comité');
    }

    public function storeBeta(Request $request)
    {
        //dd($request->all());
        $update = $this->moduleRepository->ActivateModuleBeta($request->espaces_id, $request->modules_id);

        return $this->sendResponse(null, "Mise en place de la béta");
    }
}
