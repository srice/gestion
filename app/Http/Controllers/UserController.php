<?php

namespace App\Http\Controllers;

use App\Repository\Core\UserRepository;
use Illuminate\Http\Request;
use Nahidz\Imapx\Imapx;

class UserController extends Controller
{
    private $sector;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->sector = '';
        $this->userRepository = $userRepository;
    }
    
    public function profil(Imapx $imapx)
    {
        //dd($imapx->getInbox());
        $config = (object)[
            "sector"    => $this->sector
        ];
        return view('User.profil', [
            "config"    => $config,
            "user"      => $this->userRepository->getAuthUser(),
            "inboxes"     => $imapx->getInbox()
        ]);
    }

    public function update(Request $request)
    {
        $this->userRepository->updateProfil(
            $request->name,
            $request->email,
            $request->poste
        );

    }
}
