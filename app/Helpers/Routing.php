<?php
if (!function_exists('currentRoute')) {
    function currentRoute(...$routes)
    {
        foreach($routes as $route) {
            if(request()->url() == $route) {
                return ' m-menu__item--active';
            }
        }
    }
}

if (!function_exists('stateSector')) {
    function stateSector($sector)
    {
        switch ($sector)
        {
            default: return "Bienvenue";
            case 'comite': return "Comité";
            case 'prestation': return "Prestation";
            case 'facturation': return "Facturation";
            case 'espace': return "Espace";
            case 'infrastructure': return "Infrastructure";
            case 'support': return "Support";
        }
    }
}
