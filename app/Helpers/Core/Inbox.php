<?php
if(!function_exists('getSubject')){
    function getSubject($id){
        $imapx = new \Nahidz\Imapx\Imapx();
        $msg = $imapx->readMail($id);

        return $msg->getSubject();
    }
}

if(!function_exists('getSenderName')){
    function getSenderName($id){
        $imapx = new \Nahidz\Imapx\Imapx();
        $msg = $imapx->readMail($id);

        return $msg->getSenderName();
    }
}

if(!function_exists('getSenderEmail')){
    function getSenderEmail($id){
        $imapx = new \Nahidz\Imapx\Imapx();
        $msg = $imapx->readMail($id);

        return $msg->getSenderEmail();
    }
}

if(!function_exists('isSeen')){
    function isSeen($id){
        $imapx = new \Nahidz\Imapx\Imapx();
        $msg = $imapx->readMail($id);

        return $msg->isSeen();
    }
}

if(!function_exists('getBody')){
    function getBody($id){
        $imapx = new \Nahidz\Imapx\Imapx();
        $msg = $imapx->readMail($id);

        return $msg->getBody('text', true);
    }
}

if(!function_exists('countUnseen')){
    function countUnseen(){
        $imapx = new \Nahidz\Imapx\Imapx();
        $msgs = $imapx->getInbox();

        $i = 0;

        foreach ($msgs as $msg)
        {
            if($msg->seen == 0){
                $i++;
            }
        }

        return $i;
    }
}


