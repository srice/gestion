<?php
if(!function_exists('stateNotification')){
    function stateNotification($stateNotification){
        switch ($stateNotification)
        {
            case 0: return 'm-list-timeline__badge--error';
            case 1: return 'm-list-timeline__badge--warning';
            case 2: return 'm-list-timeline__badge--success';
            case 3: return 'm-list-timeline__badge--info';
            default: return 'm-list-timeline__badge--default';
        }
    }
}
