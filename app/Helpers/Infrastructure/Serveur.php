<?php
if(!function_exists('stateServeur')){
    function stateServeur($registar, $serverIP){
        switch ($registar)
        {
            case 1:
                return '<span class="m-badge m-badge--info" data-toggle="m-tooltip" title="Le serveur est local, donc est considerer comme non mesurable">Information non disponible</span>';
            case 2:
                $connect = @fsockopen($serverIP, '80');
                if(is_resource($connect)){
                    return '<span class="m-badge m-badge--success" data-toggle="m-tooltip" title="Le serveur est disponible">OK</span>';
                }else{
                    return '<span class="m-badge m-badge--danger" data-toggle="m-tooltip" title="Le serveur est indisponible">Defaut</span>';
                }
            case 3:
                $connect = @fsockopen($serverIP, '80');
                if(is_resource($connect)){
                    return '<span class="m-badge m-badge--success" data-toggle="m-tooltip" title="Le serveur est disponible">OK</span>';
                }else{
                    return '<span class="m-badge m-badge--danger" data-toggle="m-tooltip" title="Le serveur est indisponible">Defaut</span>';
                }
        }
    }
}