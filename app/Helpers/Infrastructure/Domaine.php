<?php
if(!function_exists('stateDomaine')){
    function stateDomaine($stat){
        switch ($stat)
        {
            case 'expired': return '<i class="fa fa-times-circle icon-2x text-danger" data-toggle="m-tooltip" title="Domaine Expirer"></i>';
            case 'inCreation': return '<i class="fa fa-refresh icon-2x text-info" data-toggle="m-tooltip" title="Domaine en cours de création"></i>';
            case 'ok': return '<i class="fa fa-check-circle icon-2x text-success" data-toggle="m-tooltip" title="OK"></i>';
            case 'pendingDebt': return '<i class="fa fa-warning icon-2x text-warning" data-toggle="m-tooltip" title="En attente de paiement"></i>';
            case 'unPaid': return '<i class="fa fa-warning icon-2x text-warning" data-toggle="m-tooltip" title="Erreur de Paiement"></i>';
            default: return '<i class="fa fa-info icon-2x text-default" data-toggle="m-tooltip" title="Etat Inconnue"></i>';
        }
    }
}