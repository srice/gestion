<?php
if(!function_exists('calcForLigne')){
    function calcForLigne($montant, $qte){
        return formatCurrency($montant*$qte);
    }
}

if(!function_exists('calcPlus')){
    function calcPlus($initial, $nbr){
        return $initial + $nbr;
    }
}

if(!function_exists('calcMoins')){
    function calcMoins($initial, $nbr){
        return $initial - $nbr;
    }
}

if(!function_exists('calcMultiple')){
    function calcMultiple($initial, $nbr){
        return $initial * $nbr;
    }
}

if(!function_exists('calcDiffForSum')){
    function calcDiffForSum($initial, $nbr){
        $calc = $initial - $nbr;

        if ($calc == 0){
            return true;
        } else {
            return false;
        }
    }
}