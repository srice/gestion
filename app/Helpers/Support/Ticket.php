<?php
if(!function_exists('stateTicket')){
    function stateTicket($state){
        switch ($state)
        {
            case 0:
                return '<span class="m-badge m-badge--info"><i class="fa fa-unlock"></i> Ouvert</span>';
                break;

            case 1:
                return '<span class="m-badge m-badge--danger"><i class="fa fa-lock"></i> Clôturer</span>';
                break;

            default:
                return null;
        }
    }
}