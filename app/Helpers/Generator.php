<?php
if(!function_exists('createPassword')){
    function createPassword(){
        return str_random(8);
    }
}

if(!function_exists('createLicence')){
    function createLicence(){
        $token = "AZERTYUIOPQSDFGHJKLMWXCVBN1234567890";
        $serial = "";

        for($i=0; $i < 4; $i++){
            for ($j = 0; $j < 5; $j++) {
                $serial .= $token[rand(0, 35)];
            }

            if ($i < 3) {
                $serial .= '-';
            }
        }

        return $serial;
    }
}

if(!function_exists('generateKey')){
    function generateKey(){
        return str_random(16);
    }
}
