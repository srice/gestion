<?php

namespace App\Model\Web\Blog;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function blogs()
    {
        return $this->hasMany(Blog::class);
    }
}
