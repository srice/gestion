<?php

namespace App\Model\Facturation\Commande;

use Illuminate\Database\Eloquent\Model;

class CommandeLigne extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function commande()
    {
        return $this->belongsTo(Commande::class, 'commandes_id');
    }
}
