<?php

namespace App\Model\Facturation\Commande;

use App\Model\Comite\Comite;
use App\Model\Facturation\Contrat\Contrat;
use App\Model\Facturation\Facture\Facture;
use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    protected $guarded = [];

    public function getDates()
    {
        return ["date"];
    }

    public function comite()
    {
        return $this->belongsTo(Comite::class, 'comites_id');
    }

    public function lignes()
    {
        return $this->hasMany(CommandeLigne::class);
    }

    public function paiements()
    {
        return $this->hasMany(CommandePaiement::class);
    }

    public function contrat()
    {
        return $this->hasOne(Contrat::class);
    }

    public function facture()
    {
        return $this->hasOne(Facture::class);
    }
}
