<?php

namespace App\Model\Facturation\Commande;

use Illuminate\Database\Eloquent\Model;

class CommandePaiement extends Model
{
    protected $guarded = [];

    public function getDates()
    {
        return ['dateReglement'];
    }

    public function commande()
    {
        return $this->belongsTo(Commande::class, 'commandes_id');
    }
}
