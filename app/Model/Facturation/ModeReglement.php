<?php

namespace App\Model\Facturation;

use App\Model\Facturation\Facture\Facture;
use Illuminate\Database\Eloquent\Model;

class ModeReglement extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function factures()
    {
        return $this->hasMany(Facture::class);
    }
}
