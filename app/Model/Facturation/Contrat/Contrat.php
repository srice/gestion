<?php

namespace App\Model\Facturation\Contrat;

use App\Model\Comite\Comite;
use App\Model\Espace\Espace;
use App\Model\Facturation\Commande\Commande;
use Illuminate\Database\Eloquent\Model;

class Contrat extends Model
{
    protected $guarded = [];

    public function getDates()
    {
        return ["start", "end"];
    }

    public function comite()
    {
        return $this->belongsTo(Comite::class, 'comites_id');
    }

    public function commande()
    {
        return $this->belongsTo(Commande::class, 'commandes_id');
    }

    public function type()
    {
        return $this->belongsTo(ContratType::class, 'types_id');
    }

    public function espace()
    {
        return $this->hasOne(Espace::class);
    }
}
