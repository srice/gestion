<?php

namespace App\Model\Facturation\Contrat;

use Illuminate\Database\Eloquent\Model;

class ContratType extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function contrats()
    {
        return $this->hasMany(Contrat::class);
    }
}
