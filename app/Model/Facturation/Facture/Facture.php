<?php

namespace App\Model\Facturation\Facture;

use App\Model\Comite\Comite;
use App\Model\Facturation\Commande\Commande;
use Illuminate\Database\Eloquent\Model;

class Facture extends Model
{
    protected $guarded = [];

    public function getDates()
    {
        return ['date'];
    }

    public function commande()
    {
        return $this->belongsTo(Commande::class, 'commandes_id');
    }

    public function comite()
    {
        return $this->belongsTo(Comite::class, 'comites_id');
    }

    public function lignes()
    {
        return $this->hasMany(FactureLigne::class);
    }

    public function paiements()
    {
        return $this->hasMany(FacturePaiement::class);
    }
}
