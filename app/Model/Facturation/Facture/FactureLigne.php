<?php

namespace App\Model\Facturation\Facture;

use App\Model\Prestation\Service\Service;
use Illuminate\Database\Eloquent\Model;

class FactureLigne extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function facture()
    {
        return $this->belongsTo(Facture::class, 'factures_id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'services_id');
    }
}
