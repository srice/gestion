<?php

namespace App\Model\Facturation\Facture;

use App\Model\Comite\Comite;
use App\Model\Facturation\ModeReglement;
use Illuminate\Database\Eloquent\Model;

class FacturePaiement extends Model
{
    protected $guarded = [];

    public function getDates()
    {
        return ['dateReglement'];
    }

    public function facture()
    {
        return $this->belongsTo(Facture::class, 'factures_id');
    }

    public function comite()
    {
        return $this->belongsTo(Comite::class, 'comites_id');
    }

    public function mode()
    {
        return $this->belongsTo(ModeReglement::class, 'modes_id');
    }
}
