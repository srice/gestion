<?php

namespace App\Model\Prestation\Service;

use App\Model\Facturation\Facture\FactureLigne;
use App\Model\Prestation\Famille\FamillePrestation;
use App\Model\Prestation\Module\Module;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function famille()
    {
        return $this->belongsTo(FamillePrestation::class, 'famillesId');
    }

    public function module()
    {
        return $this->belongsTo(Module::class, 'modules_id');
    }

    public function tarifs()
    {
        return $this->hasMany(ServiceTarif::class);
    }
    public function factureligne()
    {
        return $this->hasOne(FactureLigne::class);
    }

}
