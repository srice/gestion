<?php

namespace App\Model\Prestation\Service;

use Illuminate\Database\Eloquent\Model;

class ServiceTarif extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function service()
    {
        return $this->belongsTo(Service::class, 'services_id');
    }
}
