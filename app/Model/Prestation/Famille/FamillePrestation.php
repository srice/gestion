<?php

namespace App\Model\Prestation\Famille;

use App\Model\Prestation\Service\Service;
use Illuminate\Database\Eloquent\Model;

class FamillePrestation extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function services()
    {
        return $this->hasMany(Service::class);
    }
}
