<?php

namespace App\Model\Prestation\Module;

use Illuminate\Database\Eloquent\Model;

class ModuleTask extends Model
{
    protected $guarded = [];

    public function getDates()
    {
        return ["start", "end"];
    }

    public function module()
    {
        return $this->belongsTo(Module::class, 'modules_id');
    }
}
