<?php

namespace App\Model\Prestation\Module;

use Illuminate\Database\Eloquent\Model;

class ModuleChangelog extends Model
{
    protected $guarded = [];

    public function getDates()
    {
        return ["dateChangelog"];
    }

    public function module()
    {
        return $this->belongsTo(Module::class, 'modules_id');
    }
}
