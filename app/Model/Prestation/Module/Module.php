<?php

namespace App\Model\Prestation\Module;

use App\Model\Prestation\Service\Service;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function services()
    {
        return $this->hasMany(Service::class);
    }

    public function tasks()
    {
        return $this->hasMany(ModuleTask::class, 'modules_id');
    }

    public function changelogs()
    {
        return $this->hasMany(ModuleChangelog::class, 'modules_id');
    }
}
