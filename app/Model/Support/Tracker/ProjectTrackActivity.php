<?php

namespace App\Model\Support\Tracker;

use Illuminate\Database\Eloquent\Model;

class ProjectTrackActivity extends Model
{
    protected $guarded = [];

    public function track()
    {
        return $this->belongsTo(ProjectTrack::class);
    }
}
