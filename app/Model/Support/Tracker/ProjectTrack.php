<?php

namespace App\Model\Support\Tracker;

use Illuminate\Database\Eloquent\Model;

class ProjectTrack extends Model
{
    protected $guarded = [];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function version()
    {
        return $this->belongsTo(ProjectVersion::class);
    }

    public function activities()
    {
        return $this->hasMany(ProjectTrackActivity::class);
    }
}
