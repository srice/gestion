<?php

namespace App\Model\Support\Tracker;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function versions()
    {
        return $this->hasMany(ProjectVersion::class);
    }

    public function tracks()
    {
        return $this->hasMany(ProjectTrack::class);
    }
}
