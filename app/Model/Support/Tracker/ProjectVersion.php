<?php

namespace App\Model\Support\Tracker;

use Illuminate\Database\Eloquent\Model;

class ProjectVersion extends Model
{
    protected $guarded = [];
    public function getDates()
    {
        return ["publish_at", "obsolete_at"];
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function track()
    {
        return $this->belongsTo(ProjectTrack::class);
    }
}
