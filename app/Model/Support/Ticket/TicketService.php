<?php

namespace App\Model\Support\Ticket;

use Illuminate\Database\Eloquent\Model;

class TicketService extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function univer()
    {
        return $this->belongsTo(Univer::class, 'ticket_univer_id');
    }
}
