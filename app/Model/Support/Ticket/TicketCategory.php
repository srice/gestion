<?php

namespace App\Model\Support\Ticket;

use Illuminate\Database\Eloquent\Model;

class TicketCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function support()
    {
        return $this->belongsTo(TicketSupport::class, 'ticket_support_id');
    }
}
