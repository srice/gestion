<?php

namespace App\Model\Support\Ticket;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $guarded = [];

    public function discussions()
    {
        return $this->hasMany(TicketDiscussion::class);
    }

}
