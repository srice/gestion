<?php

namespace App\Model\Support\Ticket;

use Illuminate\Database\Eloquent\Model;

class TicketSupport extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function categories()
    {
        return $this->hasMany(TicketCategory::class);
    }
}
