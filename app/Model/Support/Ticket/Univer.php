<?php

namespace App\Model\Support\Ticket;

use Illuminate\Database\Eloquent\Model;

class Univer extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
