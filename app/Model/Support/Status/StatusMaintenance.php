<?php

namespace App\Model\Support\Status;

use Illuminate\Database\Eloquent\Model;

class StatusMaintenance extends Model
{
    protected $guarded = [];

    public function getDates()
    {
        return ["schedule_at"];
    }
}
