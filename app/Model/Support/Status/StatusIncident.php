<?php

namespace App\Model\Support\Status;

use Illuminate\Database\Eloquent\Model;

class StatusIncident extends Model
{
    protected $guarded = [];

    public function component()
    {
        return $this->belongsTo(StatusComponent::class, 'status_component_id');
    }
}
