<?php

namespace App\Model\Support\Status;

use Illuminate\Database\Eloquent\Model;

class StatusComponent extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function group()
    {
        return $this->belongsTo(StatusComponentGroup::class, 'status_component_group_id');
    }

    public function incidents()
    {
        return $this->hasMany(StatusIncident::class);
    }
}
