<?php

namespace App\Model\Support\Status;

use Illuminate\Database\Eloquent\Model;

class StatusComponentGroup extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function components()
    {
        return $this->hasMany(StatusComponent::class);
    }

}
