<?php

namespace App\Model\Infrastructure\Server;

use Illuminate\Database\Eloquent\Model;

class ServeurRegistar extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
