<?php

namespace App\Model\Infrastructure\Server;

use Illuminate\Database\Eloquent\Model;

class Serveur extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
