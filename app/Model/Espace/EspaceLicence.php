<?php

namespace App\Model\Espace;

use Illuminate\Database\Eloquent\Model;

class EspaceLicence extends Model
{
    protected $guarded = [];

    public function getDates()
    {
        return ['start', 'end'];
    }

    public function espace()
    {
        return $this->belongsTo(Espace::class, 'espaces_id');
    }
}
