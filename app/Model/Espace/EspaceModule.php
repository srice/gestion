<?php

namespace App\Model\Espace;

use Illuminate\Database\Eloquent\Model;

class EspaceModule extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function espace()
    {
        return $this->belongsTo(Espace::class, 'espaces_id');
    }
}
