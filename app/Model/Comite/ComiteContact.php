<?php

namespace App\Model\Comite;

use Illuminate\Database\Eloquent\Model;

class ComiteContact extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function comite()
    {
        return $this->belongsTo(Comite::class);
    }

    public function getDates()
    {
        return ["created_at"];
    }


}
