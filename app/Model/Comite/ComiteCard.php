<?php

namespace App\Model\Comite;

use Illuminate\Database\Eloquent\Model;

class ComiteCard extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function comite()
    {
        return $this->belongsTo(Comite::class, 'customers_id', 'customersId');
    }
}
