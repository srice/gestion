<?php

namespace App\Model\Comite;

use Illuminate\Database\Eloquent\Model;

class ComitePayment extends Model
{
    protected $guarded = [];

    public function comite()
    {
        return $this->belongsTo(Comite::class, 'comitesId');
    }
}
