<?php

namespace App\Model\Comite;

use App\Model\Espace\Espace;
use App\Model\Facturation\Commande\Commande;
use App\Model\Facturation\Contrat\Contrat;
use App\Model\Facturation\Facture\Facture;
use App\Model\Facturation\Facture\FacturePaiement;
use Illuminate\Database\Eloquent\Model;

class Comite extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function contacts()
    {
        return $this->hasMany(ComiteContact::class, 'comitesId');
    }

    public function mandates()
    {
        return $this->hasMany(ComiteMandate::class, 'customersId');
    }

    public function cards()
    {
        return $this->hasMany(ComiteCard::class, 'customersId');
    }

    public function payments()
    {
        return $this->hasMany(ComitePayment::class, 'comitesId');
    }

    public function commandes()
    {
        return $this->hasMany(Commande::class);
    }

    public function factures()
    {
        return $this->hasMany(Facture::class);
    }

    public function facturepaiements()
    {
        return $this->hasMany(FacturePaiement::class);
    }

    public function contrats()
    {
        return $this->hasMany(Contrat::class);
    }

    public function espaces()
    {
        return $this->hasMany(Espace::class);
    }
}
