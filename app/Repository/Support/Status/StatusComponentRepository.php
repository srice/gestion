<?php
namespace App\Repository\Support\Status;

use App\Model\Support\Status\StatusComponent;

class StatusComponentRepository
{
    /**
     * @var StatusComponent
     */
    private $statusComponent;

    /**
     * StatusComponentRepository constructor.
     * @param StatusComponent $statusComponent
     */

    public function __construct(StatusComponent $statusComponent)
    {
        $this->statusComponent = $statusComponent;
    }

    public function list($group_id)
    {
        return $this->statusComponent->newQuery()
            ->where('status_component_group_id', $group_id)
            ->get()
            ->load('group', 'incidents');
    }

    public function get($component_id)
    {
        return $this->statusComponent->newQuery()
            ->find($component_id)
            ->load('group', 'incidents');
    }

    public function countOperationnel($group_id)
    {
        return $this->statusComponent->newQuery()
            ->where('status_component_group_id', $group_id)
            ->where('state', 0)
            ->get()
            ->count();
    }

    public function countOrange($group_id)
    {
        return $this->statusComponent->newQuery()
            ->where('status_component_group_id', $group_id)
            ->where('state', 1)
            ->orWhere('state', 2)
            ->get()
            ->count();
    }

    public function countRed($group_id)
    {
        return $this->statusComponent->newQuery()
            ->where('status_component_group_id', $group_id)
            ->where('state', 3)
            ->get()
            ->count();
    }

}

