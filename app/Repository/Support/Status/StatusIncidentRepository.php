<?php
namespace App\Repository\Support\Status;

use App\Model\Support\Status\StatusIncident;

class StatusIncidentRepository
{
    /**
     * @var StatusIncident
     */
    private $statusIncident;

    /**
     * StatusIncidentRepository constructor.
     * @param StatusIncident $statusIncident
     */

    public function __construct(StatusIncident $statusIncident)
    {

        $this->statusIncident = $statusIncident;
    }

    public function list()
    {
        return $this->statusIncident->newQuery()
            ->get()
            ->load('component');
    }

    public function get($incident_id)
    {
        return $this->statusIncident->newQuery()
            ->find($incident_id)
            ->load('component');
    }

    public function days()
    {
        return $this->statusIncident->newQuery()
            ->whereBetween('updated_at', [now()->startOfDay(), now()->endOfDay()])
            ->get()
            ->load('component');
    }

}

