<?php
namespace App\Repository\Support\Status;

use App\Model\Support\Status\StatusMaintenance;

class StatusMaintenanceRepository
{
    /**
     * @var StatusMaintenance
     */
    private $statusMaintenance;

    /**
     * StatusMaintenanceRepository constructor.
     * @param StatusMaintenance $statusMaintenance
     */

    public function __construct(StatusMaintenance $statusMaintenance)
    {

        $this->statusMaintenance = $statusMaintenance;
    }

    public function list()
    {
        return $this->statusMaintenance->newQuery()
            ->get();
    }

    public function get($maintenance_id)
    {
        return $this->statusMaintenance->newQuery()
            ->find($maintenance_id);
    }

}

