<?php
namespace App\Repository\Support\Status;

use App\Model\Support\Status\StatusComponentGroup;

class StatusComponentGroupRepository
{
    /**
     * @var StatusComponentGroup
     */
    private $statusComponentGroup;

    /**
     * StatusComponentGroupRepository constructor.
     * @param StatusComponentGroup $statusComponentGroup
     */

    public function __construct(StatusComponentGroup $statusComponentGroup)
    {
        $this->statusComponentGroup = $statusComponentGroup;
    }

    public function list()
    {
        return $this->statusComponentGroup->newQuery()
            ->get()->load('components');
    }

    public function get($group_id)
    {
        return $this->statusComponentGroup->newQuery()
            ->find($group_id)
            ->load('components');
    }

}

