<?php
namespace App\Repository\Support\Tracker\Project;

use App\Model\Support\Tracker\ProjectTrack;

class ProjectTrackRepository
{
    /**
     * @var ProjectTrack
     */
    private $projectTrack;

    /**
     * ProjectTrackRepository constructor.
     * @param ProjectTrack $projectTrack
     */

    public function __construct(ProjectTrack $projectTrack)
    {
        $this->projectTrack = $projectTrack;
    }

}

