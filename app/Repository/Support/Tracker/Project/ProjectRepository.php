<?php
namespace App\Repository\Support\Tracker\Project;

use App\Model\Support\Tracker\Project;

class ProjectRepository
{
    /**
     * @var Project
     */
    private $project;

    /**
     * ProjectRepository constructor.
     * @param Project $project
     */

    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    public function list()
    {
        return $this->project->newQuery()
            ->get()
            ->load('versions', 'tracks');
    }

}

