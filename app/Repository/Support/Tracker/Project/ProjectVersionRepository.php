<?php
namespace App\Repository\Support\Tracker\Project;

use App\Model\Support\Tracker\ProjectVersion;



class ProjectVersionRepository
{
    /**
     * @var ProjectVersion
     */
    private $projectVersion;

    /**
     * ProjectVersionRepository constructor.
     * @param ProjectVersion $projectVersion
     */

    public function __construct(ProjectVersion $projectVersion)
    {
        $this->projectVersion = $projectVersion;
    }

}

