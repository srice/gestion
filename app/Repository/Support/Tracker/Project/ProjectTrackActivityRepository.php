<?php
namespace App\Repository\Support\Tracker\Project;

use App\Model\Support\Tracker\ProjectTrackActivity;

class ProjectTrackActivityRepository
{
    /**
     * @var ProjectTrackActivity
     */
    private $projectTrackActivity;

    /**
     * ProjectTrackActivityRepository constructor.
     * @param ProjectTrackActivity $projectTrackActivity
     */

    public function __construct(ProjectTrackActivity $projectTrackActivity)
    {
        $this->projectTrackActivity = $projectTrackActivity;
    }

}

