<?php
namespace App\Repository\Support\Ticket;

use App\Model\Support\Ticket\TicketSupport;

class TicketSupportRepository
{
    /**
     * @var TicketSupport
     */
    private $ticketSupport;

    /**
     * TicketSupportRepository constructor.
     * @param TicketSupport $ticketSupport
     */

    public function __construct(TicketSupport $ticketSupport)
    {
        $this->ticketSupport = $ticketSupport;
    }

}

