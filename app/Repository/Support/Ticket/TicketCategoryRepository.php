<?php
namespace App\Repository\Support\Ticket;

use App\Model\Support\Ticket\TicketCategory;

class TicketCategoryRepository
{
    /**
     * @var TicketCategory
     */
    private $ticketCategory;

    /**
     * TicketCategoryRepository constructor.
     * @param TicketCategory $ticketCategory
     */

    public function __construct(TicketCategory $ticketCategory)
    {
        $this->ticketCategory = $ticketCategory;
    }

}

