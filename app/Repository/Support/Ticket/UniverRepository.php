<?php
namespace App\Repository\Support\Ticket;

use App\Model\Support\Ticket\Univer;

class UniverRepository
{
    /**
     * @var Univer
     */
    private $univer;

    /**
     * UniverRepository constructor.
     * @param Univer $univer
     */

    public function __construct(Univer $univer)
    {
        $this->univer = $univer;
    }

    public function all()
    {
        return $this->univer->newQuery()
            ->get();
    }

    public function countAll()
    {
        return $this->univer->newQuery()
            ->count();
    }

    public function searchByTerm($q)
    {
        return $this->univer->newQuery()
            ->where('designation', 'LIKE', "%".$q."%")
            ->get();
    }

    public function countByTerm($q)
    {
        return $this->univer->newQuery()
            ->where('designation', 'LIKE', "%".$q."%")
            ->count();
    }

}

