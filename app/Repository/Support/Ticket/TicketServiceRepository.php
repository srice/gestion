<?php
namespace App\Repository\Support\Ticket;

use App\Model\Support\Ticket\TicketService;

class TicketServiceRepository
{
    /**
     * @var TicketService
     */
    private $ticketService;

    /**
     * TicketServiceRepository constructor.
     * @param TicketService $ticketService
     */

    public function __construct(TicketService $ticketService)
    {
        $this->ticketService = $ticketService;
    }

}

