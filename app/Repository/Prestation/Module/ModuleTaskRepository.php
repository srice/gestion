<?php
namespace App\Repository\Prestation\Module;

use App\Model\Prestation\Module\ModuleTask;

class ModuleTaskRepository
{
    /**
     * @var ModuleTask
     */
    private $moduleTask;

    /**
     * ModuleTaskRepository constructor.
     * @param ModuleTask $moduleTask
     */

    public function __construct(ModuleTask $moduleTask)
    {
        $this->moduleTask = $moduleTask;
    }

    public function countForModule($modules_id)
    {
        return $this->moduleTask->newQuery()
            ->where('modules_id', $modules_id)
            ->get()
            ->count();
    }

    public function get($task_id)
    {
        return $this->moduleTask->newQuery()
            ->find($task_id);
    }

    public function list($modules_id)
    {
        return $this->moduleTask->newQuery()
            ->where('modules_id', $modules_id)
            ->get();
    }

}

