<?php
namespace App\Repository\Prestation\Module;

use App\Model\Prestation\Module\ModuleChangelog;

class ModuleChangelogRepository
{
    /**
     * @var ModuleChangelog
     */
    private $moduleChangelog;

    /**
     * ModuleChangelogRepository constructor.
     * @param ModuleChangelog $moduleChangelog
     */

    public function __construct(ModuleChangelog $moduleChangelog)
    {
        $this->moduleChangelog = $moduleChangelog;
    }

    public function countForModule($modules_id)
    {
        return $this->moduleChangelog->newQuery()
            ->where('modules_id', $modules_id)
            ->get()
            ->count();
    }

    public function get($changelog_id)
    {
        return $this->moduleChangelog->newQuery()
            ->find($changelog_id);
    }

    public function list($modules_id)
    {
        return $this->moduleChangelog->newQuery()
            ->where('modules_id', $modules_id)
            ->get();
    }

}

