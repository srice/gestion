<?php
namespace App\Repository\Prestation\Module;

use App\Model\Prestation\Module\Module;

class ModuleRepository
{
    /**
     * @var Module
     */
    private $module;

    /**
     * ModuleRepository constructor.
     * @param Module $module
     */

    public function __construct(Module $module)
    {
        $this->module = $module;
    }

    public function get($modules_id)
    {
        return $this->module->newQuery()
            ->find($modules_id)->load('tasks', 'changelogs');
    }

    public function list()
    {
        return $this->module->newQuery()
            ->get();
    }

}

