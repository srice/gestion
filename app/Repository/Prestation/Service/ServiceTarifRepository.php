<?php
namespace App\Repository\Prestation\Service;

use App\Model\Prestation\Service\ServiceTarif;

class ServiceTarifRepository
{
    /**
     * @var ServiceTarif
     */
    private $serviceTarif;

    /**
     * ServiceTarifRepository constructor.
     * @param ServiceTarif $serviceTarif
     */

    public function __construct(ServiceTarif $serviceTarif)
    {
        $this->serviceTarif = $serviceTarif;
    }

}

