<?php
namespace App\Repository\Prestation\Service;

use App\Model\Prestation\Service\Service;

class ServiceRepository
{
    /**
     * @var Service
     */
    private $service;

    /**
     * ServiceRepository constructor.
     * @param Service $service
     */

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

}

