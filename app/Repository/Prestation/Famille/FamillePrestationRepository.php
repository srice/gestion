<?php
namespace App\Repository\Prestation\Famille;

use App\Model\Prestation\Famille\FamillePrestation;

class FamillePrestationRepository
{
    /**
     * @var FamillePrestation
     */
    private $famillePrestation;

    /**
     * FamillePrestationRepository constructor.
     * @param FamillePrestation $famillePrestation
     */

    public function __construct(FamillePrestation $famillePrestation)
    {
        $this->famillePrestation = $famillePrestation;
    }

}

