<?php
namespace App\Repository\Core;

use App\User;

class UserRepository
{
    /**
     * @var User
     */
    private $user;

    /**
     * UserRepository constructor.
     * @param User $user
     */

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getAuthUser()
    {
        return $this->user->newQuery()
            ->find(auth()->user()->id);
    }

    public function updateProfil($name, $email, $poste)
    {
        return $this->user->newQuery()
            ->find(auth()->user()->id)
            ->update([
                "name"  => $name,
                "email" => $email,
                "poste" => $poste
            ]);
    }

}

