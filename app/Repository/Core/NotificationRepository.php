<?php
namespace App\Repository\Core;

use App\Model\Core\Notification;

class NotificationRepository
{
    /**
     * @var Notification
     */
    private $notification;

    /**
     * NotificationRepository constructor.
     * @param Notification $notification
     */

    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    public function createAlert($stateNotify, $local, $data)
    {
        return $this->notification->newQuery()
            ->insert([
                "users_id"      => auth()->user()->id,
                "typeNotify"    => 0,
                "stateNotify" => $stateNotify,
                "localNotify"   => $local,
                "data"          => $data,
                "created_at"    => now(),
                "updated_at"    => now()
            ]);
    }

    public function createEvent($stateNotify, $local, $data)
    {
        return $this->notification->newQuery()
            ->insert([
                "users_id"      => auth()->user()->id,
                "typeNotify"    => 1,
                "stateNotify" => $stateNotify,
                "localNotify"   => $local,
                "data"          => $data,
                "created_at"    => now(),
                "updated_at"    => now()
            ]);
    }

    public function createLog($stateNotify, $local, $data)
    {
        return $this->notification->newQuery()
            ->insert([
                "users_id"      => auth()->user()->id,
                "typeNotify"    => 2,
                "stateNotify" => $stateNotify,
                "localNotify"   => $local,
                "data"          => $data,
                "created_at"    => now(),
                "updated_at"    => now()
            ]);
    }

    /**
     * @param $stateNotify // 0: Create |1: Edit |2: Delete|3:Info
     * @param $local
     * @param $data
     * @return bool
     */
    public static function createStaticAlert($stateNotify, $local, $data)
    {
        $notification = new Notification();

        return $notification->newQuery()
            ->insert([
                "users_id"      => auth()->user()->id,
                "typeNotify"    => 0,
                "stateNotify" => $stateNotify,
                "localNotify"   => $local,
                "data"          => $data,
                "created_at"    => now(),
                "updated_at"    => now()
            ]);
    }

    /**
     * @param $stateNotify // 0: Create |1: Edit |2: Delete|3:Info
     * @param $local
     * @param $data
     * @return bool
     */
    public static function createStaticEvent($stateNotify, $local, $data)
    {
        $notification = new Notification();

        return $notification->newQuery()
            ->insert([
                "users_id"      => auth()->user()->id,
                "typeNotify"    => 1,
                "stateNotify" => $stateNotify,
                "localNotify"   => $local,
                "data"          => $data,
                "created_at"    => now(),
                "updated_at"    => now()
            ]);
    }

    /**
     * @param $stateNotify // 0: Create |1: Edit |2: Delete|3:Info
     * @param $local
     * @param $data
     * @return bool
     */
    public static function createStaticLog($stateNotify, $local, $data)
    {
        $notification = new Notification();

        return $notification->newQuery()
            ->insert([
                "users_id"      => auth()->user()->id,
                "typeNotify"    => 2,
                "stateNotify" => $stateNotify,
                "localNotify"   => $local,
                "data"          => $data,
                "created_at"    => now(),
                "updated_at"    => now()
            ]);
    }

    public function countAlert()
    {
        return $this->notification->newQuery()
            ->where('typeNotify', 0)
            ->get()
            ->count();
    }

    public function getAlert()
    {
        return $this->notification->newQuery()
            ->where('typeNotify', 0)
            ->get();
    }

    public function countEvent()
    {
        return $this->notification->newQuery()
            ->where('typeNotify', 1)
            ->get()
            ->count();
    }

    public function getEvent()
    {
        return $this->notification->newQuery()
            ->where('typeNotify', 1)
            ->get();
    }

    public function countLogs()
    {
        return $this->notification->newQuery()
            ->where('typeNotify', 2)
            ->get()
            ->count();
    }

    public function getLogs()
    {
        return $this->notification->newQuery()
            ->where('typeNotify', 2)
            ->get();
    }

    public function countNotif()
    {
        return $this->notification->newQuery()
            ->get()
            ->count();
    }

    public function cleanNotification()
    {
        return $this->notification->newQuery()
            ->delete();
    }

}

