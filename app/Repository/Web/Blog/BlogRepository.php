<?php
namespace App\Repository\Web\Blog;

use App\Model\Web\Blog\Blog;

class BlogRepository
{
    /**
     * @var Blog
     */
    private $blog;

    /**
     * BlogRepository constructor.
     * @param Blog $blog
     */

    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }

}

