<?php
namespace App\Repository\Web\Blog;

use App\Model\Web\Blog\BlogCategory;

class BlogCategoryRepository
{
    /**
     * @var BlogCategory
     */
    private $blogCategory;

    /**
     * BlogCategoryRepository constructor.
     * @param BlogCategory $blogCategory
     */

    public function __construct(BlogCategory $blogCategory)
    {
        $this->blogCategory = $blogCategory;
    }

}

