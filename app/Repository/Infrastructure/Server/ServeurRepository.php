<?php
namespace App\Repository\Infrastructure\Server;

use App\Model\Infrastructure\Server\Serveur;

class ServeurRepository
{
    /**
     * @var Serveur
     */
    private $serveur;

    /**
     * ServeurRepository constructor.
     * @param Serveur $serveur
     */

    public function __construct(Serveur $serveur)
    {
        $this->serveur = $serveur;
    }

}

