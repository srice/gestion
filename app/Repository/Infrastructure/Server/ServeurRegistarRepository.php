<?php
namespace App\Repository\Infrastructure\Server;

use App\Model\Infrastructure\Server\ServeurRegistar;

class ServeurRegistarRepository
{
    /**
     * @var ServeurRegistar
     */
    private $serveurRegistar;

    /**
     * ServeurRegistarRepository constructor.
     * @param ServeurRegistar $serveurRegistar
     */

    public function __construct(ServeurRegistar $serveurRegistar)
    {
        $this->serveurRegistar = $serveurRegistar;
    }

}

