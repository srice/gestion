<?php
namespace App\Repository\Comite;

use App\Model\Comite\ComiteMandate;

class ComiteMandateRepository
{
    /**
     * @var ComiteMandate
     */
    private $comiteMandate;

    /**
     * ComiteMandateRepository constructor.
     * @param ComiteMandate $comiteMandate
     */

    public function __construct(ComiteMandate $comiteMandate)
    {
        $this->comiteMandate = $comiteMandate;
    }

}

