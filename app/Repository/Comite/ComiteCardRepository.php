<?php
namespace App\Repository\Comite;

use App\Model\Comite\ComiteCard;

class ComiteCardRepository
{
    /**
     * @var ComiteCard
     */
    private $comiteCard;

    /**
     * ComiteCardRepository constructor.
     * @param ComiteCard $comiteCard
     */

    public function __construct(ComiteCard $comiteCard)
    {
        $this->comiteCard = $comiteCard;
    }

}

