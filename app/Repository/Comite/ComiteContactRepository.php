<?php
namespace App\Repository\Comite;

use App\Model\Comite\ComiteContact;

class ComiteContactRepository
{
    /**
     * @var ComiteContact
     */
    private $comiteContact;

    /**
     * ComiteContactRepository constructor.
     * @param ComiteContact $comiteContact
     */

    public function __construct(ComiteContact $comiteContact)
    {
        $this->comiteContact = $comiteContact;
    }

    public function create($comitesId, $prenom, $nom, $email, $position, $telephone)
    {
        return $this->comiteContact->newQuery()
            ->create([
                "comitesId"     => $comitesId,
                "prenom"        => $prenom,
                "nom"           => $nom,
                "email"         => $email,
                "position"      => $position,
                "telephone"     => $telephone
            ]);
    }

}

