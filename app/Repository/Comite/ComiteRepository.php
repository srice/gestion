<?php
namespace App\Repository\Comite;

use App\Model\Comite\Comite;

class ComiteRepository
{
    /**
     * @var Comite
     */
    private $comite;

    /**
     * ComiteRepository constructor.
     * @param Comite $comite
     */

    public function __construct(Comite $comite)
    {
        $this->comite = $comite;
    }

    public function list()
    {
        return $this->comite->newQuery()
            ->get();
    }

    public function create($name, $adresse, $cp, $ville, $tel, $email)
    {
        return $this->comite->newQuery()
            ->create([
                "name"      => $name,
                "adresse"   => $adresse,
                "cp"        => $cp,
                "ville"     => $ville,
                "tel"       => $tel,
                "email"     => $email,
                "password"  => createPassword()
            ]);
    }

    public function get($id)
    {
        return $this->comite->newQuery()
            ->find($id)
            ->load('contacts');
    }

    public function getForApi($id)
    {
        return $this->comite->newQuery()
            ->find($id);
    }

}

