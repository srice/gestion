<?php
namespace App\Repository\Comite;

use App\Model\Comite\ComitePayment;

class ComitePaymentRepository
{
    /**
     * @var ComitePayment
     */
    private $comitePayment;

    /**
     * ComitePaymentRepository constructor.
     * @param ComitePayment $comitePayment
     */

    public function __construct(ComitePayment $comitePayment)
    {
        $this->comitePayment = $comitePayment;
    }

}

