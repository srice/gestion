<?php
namespace App\Repository\Facturation;

use App\Model\Facturation\ModeReglement;

class ModeReglementRepository
{
    /**
     * @var ModeReglement
     */
    private $modeReglement;

    /**
     * ModeReglementRepository constructor.
     * @param ModeReglement $modeReglement
     */

    public function __construct(ModeReglement $modeReglement)
    {
        $this->modeReglement = $modeReglement;
    }

}

