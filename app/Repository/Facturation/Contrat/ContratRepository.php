<?php
namespace App\Repository\Facturation\Contrat;

use App\Model\Facturation\Contrat\Contrat;

class ContratRepository
{
    /**
     * @var Contrat
     */
    private $contrat;

    /**
     * ContratRepository constructor.
     * @param Contrat $contrat
     */

    public function __construct(Contrat $contrat)
    {
        $this->contrat = $contrat;
    }

}

