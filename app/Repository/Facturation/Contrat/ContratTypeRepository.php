<?php
namespace App\Repository\Facturation\Contrat;

use App\Model\Facturation\Contrat\ContratType;

class ContratTypeRepository
{
    /**
     * @var ContratType
     */
    private $contratType;

    /**
     * ContratTypeRepository constructor.
     * @param ContratType $contratType
     */

    public function __construct(ContratType $contratType)
    {

        $this->contratType = $contratType;
    }

}

