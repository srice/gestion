<?php
namespace App\Repository\Facturation\Facture;

use App\Model\Facturation\Facture\FactureLigne;

class FactureLigneRepository
{
    /**
     * @var FactureLigne
     */
    private $factureLigne;

    /**
     * FactureLigneRepository constructor.
     * @param FactureLigne $factureLigne
     */

    public function __construct(FactureLigne $factureLigne)
    {
        $this->factureLigne = $factureLigne;
    }

}

