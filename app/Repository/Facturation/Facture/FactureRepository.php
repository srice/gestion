<?php
namespace App\Repository\Facturation\Facture;

use App\Model\Facturation\Facture\Facture;

class FactureRepository
{
    /**
     * @var Facture
     */
    private $facture;

    /**
     * FactureRepository constructor.
     * @param Facture $facture
     */

    public function __construct(Facture $facture)
    {
        $this->facture = $facture;
    }

}

