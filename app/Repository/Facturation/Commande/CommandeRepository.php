<?php
namespace App\Repository\Facturation\Commande;

use App\Model\Facturation\Commande\Commande;

class CommandeRepository
{
    /**
     * @var Commande
     */
    private $commande;

    /**
     * CommandeRepository constructor.
     * @param Commande $commande
     */

    public function __construct(Commande $commande)
    {
        $this->commande = $commande;
    }

}

