<?php
namespace App\Repository\Facturation\Commande;

use App\Model\Facturation\Commande\CommandePaiement;

class CommandePaiementRepository
{
    /**
     * @var CommandePaiement
     */
    private $commandePaiement;

    /**
     * CommandePaiementRepository constructor.
     * @param CommandePaiement $commandePaiement
     */

    public function __construct(CommandePaiement $commandePaiement)
    {
        $this->commandePaiement = $commandePaiement;
    }

}

