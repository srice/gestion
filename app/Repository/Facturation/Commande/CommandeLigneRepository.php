<?php
namespace App\Repository\Facturation\Commande;

use App\Model\Facturation\Commande\CommandeLigne;

class CommandeLigneRepository
{
    /**
     * @var CommandeLigne
     */
    private $commandeLigne;

    /**
     * CommandeLigneRepository constructor.
     * @param CommandeLigne $commandeLigne
     */

    public function __construct(CommandeLigne $commandeLigne)
    {

        $this->commandeLigne = $commandeLigne;
    }

}

