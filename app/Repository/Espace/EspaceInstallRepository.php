<?php
namespace App\Repository\Espace;

use App\Model\Espace\EspaceInstall;

class EspaceInstallRepository
{
    /**
     * @var EspaceInstall
     */
    private $espaceInstall;

    /**
     * EspaceInstallRepository constructor.
     * @param EspaceInstall $espaceInstall
     */

    public function __construct(EspaceInstall $espaceInstall)
    {

        $this->espaceInstall = $espaceInstall;
    }

}

