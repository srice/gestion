<?php
namespace App\Repository\Espace;

use App\Model\Espace\EspaceService;

class EspaceServiceRepository
{
    /**
     * @var EspaceService
     */
    private $espaceService;

    /**
     * EspaceServiceRepository constructor.
     * @param EspaceService $espaceService
     */

    public function __construct(EspaceService $espaceService)
    {
        $this->espaceService = $espaceService;
    }

}

