<?php
namespace App\Repository\Espace;

use App\Model\Espace\Espace;

class EspaceRepository
{
    /**
     * @var Espace
     */
    private $espace;

    /**
     * EspaceRepository constructor.
     * @param Espace $espace
     */

    public function __construct(Espace $espace)
    {
        $this->espace = $espace;
    }

    public function getByComite($comite_id)
    {
        return $this->espace->newQuery()
            ->where('comites_id', $comite_id)
            ->first()
            ->load('contrat', 'service', 'installs', 'licences', 'modules');
    }


}

