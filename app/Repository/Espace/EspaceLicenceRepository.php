<?php
namespace App\Repository\Espace;

use App\Model\Espace\EspaceLicence;

class EspaceLicenceRepository
{
    /**
     * @var EspaceLicence
     */
    private $espaceLicence;

    /**
     * EspaceLicenceRepository constructor.
     * @param EspaceLicence $espaceLicence
     */

    public function __construct(EspaceLicence $espaceLicence)
    {
        $this->espaceLicence = $espaceLicence;
    }

}

