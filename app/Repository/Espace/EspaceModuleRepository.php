<?php
namespace App\Repository\Espace;

use App\Model\Espace\EspaceModule;

class EspaceModuleRepository
{
    /**
     * @var EspaceModule
     */
    private $espaceModule;

    /**
     * EspaceModuleRepository constructor.
     * @param EspaceModule $espaceModule
     */

    public function __construct(EspaceModule $espaceModule)
    {
        $this->espaceModule = $espaceModule;
    }

    public function ActivateModuleBeta($espaces_id, $modules_id)
    {
        try {
            $this->espaceModule->newQuery()
                ->where('espaces_id', $espaces_id)
                ->where('modules_id', $modules_id)
                ->first()
                ->update([
                    "beta_access"   => 1,
                    "beta_access_key"   => generateKey(),
                ]);
            return null;
        }catch (\Exception $exception)
        {
            return $exception->getMessage();
        }

    }

}

