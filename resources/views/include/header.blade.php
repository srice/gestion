<!-- begin::Header -->
<header id="m_header" class="m-grid__item m-header " m-minimize="minimize" m-minimize-offset="200" m-minimize-mobile-offset="200">
    <div class="m-header__top">
        <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
            <div class="m-stack m-stack--ver m-stack--desktop">

                <!-- begin::Brand -->
                <div class="m-stack__item m-brand">
                    <div class="m-stack m-stack--ver m-stack--general m-stack--inline">
                        <div class="m-stack__item m-stack__item--middle m-brand__logo">
                            <a href="{{ route("home") }}" class="m-brand__logo-wrapper">
                                <img alt="" src="/assets/demo/demo2/media/img/logo/logo.png" />
                            </a>
                        </div>
                        <div class="m-stack__item m-stack__item--middle m-brand__tools">
                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-left m-dropdown--align-push" m-dropdown-toggle="click" aria-expanded="true">
                                <a href="#" class="dropdown-toggle m-dropdown__toggle btn btn-outline-metal m-btn  m-btn--icon m-btn--pill">
                                    <span>{{ stateSector($config->sector) }}</span>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav">
                                                    <li class="m-nav__item">
                                                        <a href="{{ route('home') }}" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-home"></i>
                                                            <span class="m-nav__link-text">Gestionnaire</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="{{ route("Comite.index") }}" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-buildings"></i>
                                                            <span class="m-nav__link-text">Comité</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-open-box"></i>
                                                            <span class="m-nav__link-text">Prestation</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-cart"></i>
                                                            <span class="m-nav__link-text">Facturation</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-grid-menu"></i>
                                                            <span class="m-nav__link-text">Espace</span>
                                                        </a>
                                                    </li>
                                                    <!--<li class="m-nav__separator m-nav__separator--fit">
                                                    </li>-->
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-alarm"></i>
                                                            <span class="m-nav__link-text">Infrastructure</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                            <span class="m-nav__link-text">Support</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- begin::Responsive Header Menu Toggler-->
                            <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <!-- end::Responsive Header Menu Toggler-->
                        </div>
                    </div>
                </div>

                <!-- end::Brand -->

                <!-- begin::Topbar -->
                <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                                    m-dropdown-toggle="click">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
													<span class="m-topbar__userpic m--hide">
														<img src="assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless m--img-centered" alt="" />
													</span>
                                        <span class="m-topbar__welcome">Bonjour,&nbsp;</span>
                                        <span class="m-topbar__username">{{ auth()->user()->name }}</span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center" style="background: url(/assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
                                                <div class="m-card-user m-card-user--skin-dark">
                                                    <div class="m-card-user__pic">
                                                        <img src="/assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" alt="" />
                                                    </div>
                                                    <div class="m-card-user__details">
                                                        <span class="m-card-user__name m--font-weight-500">{{ auth()->user()->name }}</span>
                                                        <a href="" class="m-card-user__email m--font-weight-300 m-link">{{ auth()->user()->email }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav m-nav--skin-light">
                                                        <li class="m-nav__section m--hide">
                                                            <span class="m-nav__section-text">Section</span>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="{{ route('User.profil') }}" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                <span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">Mon Profil</span>
																		<!--<span class="m-nav__link-badge"><span class="m-badge m-badge--success">2</span></span>-->
																	</span>
																</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="profile.html" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-share"></i>
                                                                <span class="m-nav__link-text">Activités</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="profile.html" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-envelope"></i>
                                                                <span class="m-nav__link-text">Messages</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="{{ route('logout') }}" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Déconnexion</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width" m-dropdown-toggle="click"
                                    m-dropdown-persistent="1">
                                    <a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
                                        <span id="dot-notif">

                                        </span>
                                        <span class="m-nav__link-icon">
														<span class="m-nav__link-icon-wrapper">
															<i data-count="0" class="flaticon-alarm notification-icon"></i>
														</span>
													</span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center" style="background: url(/assets/app/media/img/misc/notification_bg.jpg); background-size: cover;">
                                                <span class="m-dropdown__header-title" id="countNotif"></span>
                                                <span class="m-dropdown__header-subtitle">Notification</span>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
                                                        <li class="nav-item m-tabs__item">
                                                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#alert" role="tab">
                                                                Alertes
                                                            </a>
                                                        </li>
                                                        <li class="nav-item m-tabs__item">
                                                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#event" role="tab">Evènements</a>
                                                        </li>
                                                        <li class="nav-item m-tabs__item">
                                                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#logs" role="tab">Log Système</a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="alert" role="tabpanel">
                                                            <div class="m-scrollable" data-scrollable="true" data-height="250" data-mobile-height="200">
                                                                <div class="m-list-timeline m-list-timeline--skin-light">
                                                                    <div class="m-list-timeline__items" id="timeline-alert">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane active" id="event" role="tabpanel">
                                                            <div class="m-scrollable" data-scrollable="true" data-height="250" data-mobile-height="200">
                                                                <div class="m-list-timeline m-list-timeline--skin-light">
                                                                    <div class="m-list-timeline__items" id="timeline-event">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane active" id="logs" role="tabpanel">
                                                            <div class="m-scrollable" data-scrollable="true" data-height="250" data-mobile-height="200">
                                                                <div class="m-list-timeline m-list-timeline--skin-light">
                                                                    <div class="m-list-timeline__items" id="timeline-logs">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="m-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light"
                                    m-dropdown-toggle="click">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
                                        <span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
                                        <span class="m-nav__link-icon">
														<span class="m-nav__link-icon-wrapper">
															<i class="flaticon-share"></i>
														</span>
													</span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center" style="background: url(assets/app/media/img/misc/quick_actions_bg.jpg); background-size: cover;">
                                                <span class="m-dropdown__header-title">Quick Actions</span>
                                                <span class="m-dropdown__header-subtitle">Shortcuts</span>
                                            </div>
                                            <div class="m-dropdown__body m-dropdown__body--paddingless">
                                                <div class="m-dropdown__content">
                                                    <div class="m-scrollable" data-scrollable="false" data-height="380" data-mobile-height="200">
                                                        <div class="m-nav-grid m-nav-grid--skin-light">
                                                            <div class="m-nav-grid__row">
                                                                <a href="#" class="m-nav-grid__item">
                                                                    <i class="m-nav-grid__icon flaticon-file"></i>
                                                                    <span class="m-nav-grid__text">Generate Report</span>
                                                                </a>
                                                                <a href="#" class="m-nav-grid__item">
                                                                    <i class="m-nav-grid__icon flaticon-time"></i>
                                                                    <span class="m-nav-grid__text">Add New Event</span>
                                                                </a>
                                                            </div>
                                                            <div class="m-nav-grid__row">
                                                                <a href="#" class="m-nav-grid__item">
                                                                    <i class="m-nav-grid__icon flaticon-folder"></i>
                                                                    <span class="m-nav-grid__text">Create New Task</span>
                                                                </a>
                                                                <a href="#" class="m-nav-grid__item">
                                                                    <i class="m-nav-grid__icon flaticon-clipboard"></i>
                                                                    <span class="m-nav-grid__text">Completed Tasks</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- end::Topbar -->
            </div>
        </div>
    </div>
    <div class="m-header__bottom">
        <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
            <div class="m-stack m-stack--ver m-stack--desktop">

                <!-- begin::Horizontal Menu -->
                <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                    <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light ">
                        @if(empty($config->sector))
                            <ul class="m-menu__nav  m-menu__nav--submenu-arrow">
                                <li class="m-menu__item {{ currentRoute(route('home')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('home') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Tableau de Bord
                                    </span>
                                    </a>
                                </li>
                            </ul>
                        @endif
                        @if($config->sector == 'comite')
                            <ul class="m-menu__nav  m-menu__nav--submenu-arrow">
                                <li class="m-menu__item {{ currentRoute(route('Comite.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Comite.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Tableau de Bord
                                    </span>
                                    </a>
                                </li>
                            </ul>
                        @endif
                        @if($config->sector == 'prestation')
                            <ul class="m-menu__nav  m-menu__nav--submenu-arrow">
                                <li class="m-menu__item {{ currentRoute(route('Prestation.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Prestation.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Tableau de Bord
                                    </span>
                                    </a>
                                </li>
                                <li class="m-menu__item {{ currentRoute(route('Prestation.Famille.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Prestation.Famille.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Famille de service
                                    </span>
                                    </a>
                                </li>
                                <li class="m-menu__item {{ currentRoute(route('Prestation.Service.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Prestation.Service.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Services
                                    </span>
                                    </a>
                                </li>
                                <li class="m-menu__item {{ currentRoute(route('Prestation.Module.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Prestation.Module.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Modules
                                    </span>
                                    </a>
                                </li>
                            </ul>
                        @endif
                        @if($config->sector == 'facturation')
                            <ul class="m-menu__nav  m-menu__nav--submenu-arrow">
                                <li class="m-menu__item {{ currentRoute(route('Facturation.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Facturation.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Tableau de Bord
                                    </span>
                                    </a>
                                </li>
                                <li class="m-menu__item {{ currentRoute(route('Facturation.Commande.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Facturation.Commande.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Commande
                                    </span>
                                    </a>
                                </li>
                                <li class="m-menu__item {{ currentRoute(route('Facturation.Facture.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Facturation.Facture.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Facture
                                    </span>
                                    </a>
                                </li>
                                <li class="m-menu__item {{ currentRoute(route('Facturation.Contrat.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Facturation.Contrat.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Contrat
                                    </span>
                                    </a>
                                </li>
                            </ul>
                        @endif
                        @if($config->sector == 'espace')
                            <ul class="m-menu__nav  m-menu__nav--submenu-arrow">
                                <li class="m-menu__item {{ currentRoute(route('Espace.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Espace.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Tableau de Bord
                                    </span>
                                    </a>
                                </li>
                            </ul>
                        @endif
                        @if($config->sector == 'infrastructure')
                            <ul class="m-menu__nav  m-menu__nav--submenu-arrow">
                                <li class="m-menu__item {{ currentRoute(route('Infrastructure.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Infrastructure.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Tableau de Bord
                                    </span>
                                    </a>
                                </li>
                                <li class="m-menu__item {{ currentRoute(route('Server.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Server.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Serveur
                                    </span>
                                    </a>
                                </li>
                                <li class="m-menu__item {{ currentRoute(route('Domaine.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Domaine.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Domaine
                                    </span>
                                    </a>
                                </li>
                            </ul>
                        @endif
                        @if($config->sector == 'support')
                            <ul class="m-menu__nav  m-menu__nav--submenu-arrow">
                                <li class="m-menu__item {{ currentRoute(route('Support.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Support.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Tableau de Bord
                                    </span>
                                    </a>
                                </li>
                                <li class="m-menu__item {{ currentRoute(route('Ticket.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Ticket.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Tickets
                                    </span>
                                    </a>
                                </li>
                                <li class="m-menu__item {{ currentRoute(route('Status.index')) }}"  aria-haspopup="true">
                                    <a  href="{{ route('Status.index') }}" class="m-menu__link ">
                                        <span class="m-menu__item-here"></span>
                                        <span class="m-menu__link-text">
                                        Travaux
                                    </span>
                                    </a>
                                </li>
                            </ul>
                        @endif
                    </div>
                </div>

                <!-- end::Horizontal Menu -->

                <!--begin::Search-->
                <div class="m-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-dropdown--skin-light m-header-search m-header-search--expandable m-header-search--skin-" id="m_quicksearch"
                     m-quicksearch-mode="default">

                    <!--begin::Search Form -->
                    <form class="m-header-search__form">
                        <div class="m-header-search__wrapper">
										<span class="m-header-search__icon-search" id="m_quicksearch_search">
											<i class="la la-search"></i>
										</span>
                            <span class="m-header-search__input-wrapper">
											<input autocomplete="off" type="text" name="q" class="m-header-search__input" value="" placeholder="Rechercher" id="m_quicksearch_input">
										</span>
                            <span class="m-header-search__icon-close" id="m_quicksearch_close">
											<i class="la la-remove"></i>
										</span>
                            <span class="m-header-search__icon-cancel" id="m_quicksearch_cancel">
											<i class="la la-remove"></i>
										</span>
                        </div>
                    </form>

                    <!--end::Search Form -->

                    <!--begin::Search Results -->
                    <div class="m-dropdown__wrapper">
                        <div class="m-dropdown__arrow m-dropdown__arrow--center"></div>
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-height="300" data-mobile-height="200">
                                    <div class="m-dropdown__content m-list-search m-list-search--skin-light">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end::Search Results -->
                </div>

                <!--end::Search-->
            </div>
        </div>
    </div>
</header>

<!-- end::Header -->
