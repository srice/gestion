@extends('layouts.template')

@section("css")

@endsection

@section("subheader")
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Comité - Ajout d'un comité</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
@endsection

@section('content')
    <div class="m-content">
        <!--Begin::Main Portlet-->
        <div class="m-portlet m-portlet--full-height">

            <!--begin: Portlet Head-->
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Enregistrement d'un comité
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" data-toggle="m-tooltip" class="m-portlet__nav-link m-portlet__nav-link--icon" data-direction="left" data-width="auto" title="Get help with filling up this form">
                                <i class="flaticon-info m--icon-font-size-lg3"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <!--end: Portlet Head-->

            <!--begin: Form Wizard-->
            <div class="m-wizard m-wizard--2 m-wizard--success" id="m_wizard">

                <!--begin: Message container -->
                <div class="m-portlet__padding-x">

                    <!-- Here you can put a message or alert -->
                </div>

                <!--end: Message container -->

                <!--begin: Form Wizard Head -->
                <div class="m-wizard__head m-portlet__padding-x">

                    <!--begin: Form Wizard Progress -->
                    <div class="m-wizard__progress">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>

                    <!--end: Form Wizard Progress -->

                    <!--begin: Form Wizard Nav -->
                    <div class="m-wizard__nav">
                        <div class="m-wizard__steps">
                            <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                                <a href="#" class="m-wizard__step-number">
                                    <span><i class="fa  flaticon-placeholder"></i></span>
                                </a>
                                <div class="m-wizard__step-info">
                                    <div class="m-wizard__step-title">
                                        1. Information de comité
                                    </div>
                                    <div class="m-wizard__step-desc">
                                        Inscription des informations<br>
                                        des comités
                                    </div>
                                </div>
                            </div>
                            <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
                                <a href="#" class="m-wizard__step-number">
                                    <span><i class="fa  flaticon-layers"></i></span>
                                </a>
                                <div class="m-wizard__step-info">
                                    <div class="m-wizard__step-title">
                                        2. Premier contact
                                    </div>
                                    <div class="m-wizard__step-desc">
                                        Inscription du premier contact
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end: Form Wizard Nav -->
                </div>

                <!--end: Form Wizard Head -->

                <!--begin: Form Wizard Form-->
                <div class="m-wizard__form">

                    <!--
1) Use m-form--label-align-left class to alight the form input lables to the right
2) Use m-form--state class to highlight input control borders on form validation
-->
                    <form class="m-form m-form--label-align-left- m-form--state-" id="m_form" action="{{ route('Comite.store') }}" method="POST">

                        <!--begin: Form Body -->
                        <div class="m-portlet__body">

                            <!--begin: Form Wizard Step 1-->
                            <div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">Information comité</h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">* Nom du comité:</label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="text" name="name" class="form-control m-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">* Adresse:</label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="text" name="adresse" class="form-control m-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">* Code Postal:</label>
                                                <div class="col-xl-2 col-lg-2">
                                                    <input type="text" name="cp" class="form-control m-input" placeholder="">
                                                </div>
                                                <label class="col-xl-3 col-lg-3 col-form-label">* Ville:</label>
                                                <div class="col-xl-2 col-lg-2">
                                                    <input type="text" name="ville" class="form-control m-input" placeholder="">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="m-separator m-separator--dashed m-separator--lg"></div>
                                        <div class="m-form__section">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">
                                                    Coordonnées
                                                    <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="Some help text goes here"></i>
                                                </h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">* Téléphone:</label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="tel" name="tel" class="form-control m-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">* Email:</label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="email" name="email" class="form-control m-input" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--end: Form Wizard Step 1-->

                            <!--begin: Form Wizard Step 2-->
                            <div class="m-wizard__form-step" id="m_wizard_form_step_2">
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">Contact Principal</h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">* Nom du contact:</label>
                                                <div class="col-xl-3 col-lg-3">
                                                    <input type="text" name="nom" class="form-control m-input" placeholder="">
                                                </div>
                                                <label class="col-xl-3 col-lg-3 col-form-label">* Prénom du contact:</label>
                                                <div class="col-xl-3 col-lg-3">
                                                    <input type="text" name="prenom" class="form-control m-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">* Adresse Mail du contact:</label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="email" name="email_contact" class="form-control m-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">* Position du contact:</label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="text" name="position" class="form-control m-input" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">* Téléphone du contact:</label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="text" name="tel_contact" class="form-control m-input" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--end: Form Wizard Step 2-->
                        </div>

                        <!--end: Form Body -->

                        <!--begin: Form Actions -->
                        <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 m--align-left">
                                        <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
															<span>
																<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																<span>Retour</span>
															</span>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 m--align-right">
                                        <a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
															<span>
																<i class="la la-check"></i>&nbsp;&nbsp;
																<span>Soumettre</span>
															</span>
                                        </a>
                                        <a href="#" class="btn btn-warning m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
															<span>
																<span>Sauvegarder & Continuer</span>&nbsp;&nbsp;
																<i class="la la-arrow-right"></i>
															</span>
                                        </a>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </div>
                        </div>

                        <!--end: Form Actions -->
                    </form>
                </div>

                <!--end: Form Wizard Form-->
            </div>

            <!--end: Form Wizard-->
        </div>

        <!--End::Main Portlet-->
    </div>
@endsection

@section("script")
    <script>
        (function ($) {
            let WizardDemo = function () {
                $("#m_wizard");
                let e, r, i = $("#m_form");
                return {
                    init: function () {
                        let n;
                        $("#m_wizard"), i = $("#m_form"), (r = new mWizard("m_wizard", {
                            startStep: 1
                        })).on("beforeNext", function (r) {
                            !0 !== e.form() && r.stop()
                        }), r.on("change", function (e) {
                            mUtil.scrollTop()
                        }), r.on("change", function (e) {
                            1 === e.getStep() && alert(1)
                        }), e = i.validate({
                            ignore: ":hidden",
                            rules: {
                                name: {
                                    required: true
                                },
                                adresse: {
                                    required: true
                                },
                                cp: {
                                    required: true,
                                    maxlength: 5,
                                    minlength: 5
                                },
                                ville: {
                                    required: true
                                },
                                tel: {
                                    required: true
                                },
                                email: {
                                    required: true,
                                    email: true
                                },
                                nom: {
                                    required: true
                                },
                                prenom: {
                                    required: true
                                },
                                email_contact: {
                                    required: true,
                                    email: true
                                },
                                position: {
                                    required: true
                                },
                                tel_contact: {
                                    required: true
                                }
                            },
                            messages: {
                                "name": {
                                    required: "Le nom du comité est obligatoire !"
                                },
                                "adresse": {
                                    required: "L'adresse du comité est obligatoire !"
                                },
                                "cp": {
                                    required: "Le code postal est obligatoire !",
                                    maxlength: "Le code Postal doit être de 5 caractères !",
                                    minlength: "Le code Postal doit être de 5 caractères !"
                                },
                                "ville": {
                                    required: "La ville est obligatoire !"
                                },
                                "tel": {
                                    required: "Le numéro de téléphone du comité est obligatoire !"
                                },
                                "email": {
                                    required: "L'adresse Mail du comité est obligatoire !",
                                    email: "Ce champs est de type mail et doit comporter une adresse mail valide !"
                                },
                                "nom": {
                                    required: "Le nom du contact est obligatoire !"
                                },
                                "prenom": {
                                    required: "Le prénom du contact est obligatoire !"
                                },
                                "email_contact": {
                                    required: "L'adresse mail du contact est obligatoire !",
                                    email: "Ce champs est de type mail et doit comporter une adresse mail valide !"
                                },
                                "position": {
                                    required: "La position (Poste) du contact est obligatoire !"
                                },
                                "tel_contact": {
                                    required: "Le numéro de téléphone du contact est obligatoire !"
                                },
                            },
                            invalidHandler: function (e, r) {
                                mUtil.scrollTop(), swal({
                                    title: "",
                                    text: "Il y a des erreurs dans votre soumission. S'il vous plaît corrigez-les.",
                                    type: "error",
                                    confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                                })
                            },
                            submitHandler: function (e) {
                            }
                        }), (n = i.find('[data-wizard-action="submit"]')).on("click", function (r) {
                            r.preventDefault(), e.form() && (mApp.progress(n), i.ajaxSubmit({
                                success: function () {
                                    mApp.unprogress(n), swal({
                                        title: "Création d'un comité",
                                        text: "Le formulaire à bien été soumis",
                                        type: "success",
                                        confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                                    })
                                    window.setTimeout(function () {
                                        window.location.href='/comite'
                                    }, 1500);
                                },
                                error: function (data) {
                                    console.log(data)
                                }
                            }))
                        })
                    }
                }
            }();
            jQuery(document).ready(function() {
                WizardDemo.init()
            });
        })(jQuery)
    </script>
@endsection
