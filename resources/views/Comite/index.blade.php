@extends('layouts.template')

@section("css")
    <link href="/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("subheader")
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Comité - Liste des comités</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
@endsection

@section('content')
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Liste des comités
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route("Comite.create") }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
												<span>
													<i class="la la-plus"></i>
													<span>Nouveau comité</span>
												</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Comité</th>
                        <th>Adresse</th>
                        <th>Coordonnée</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($comites as $comite)
                    <tr>
                        <td>{{ $comite->id }}</td>
                        <td>{{ $comite->name }}</td>
                        <td>
                            {{ $comite->adresse }}<br>
                            {{ $comite->cp }} {{ $comite->ville }}
                        </td>
                        <td>
                            <strong><i class="la la-phone"></i> </strong>: {{ $comite->tel }}<br>
                            <strong><i class="la la-envelope"></i> </strong>: {{ $comite->email }}<br>
                        </td>
                        <td>
                            <a href="{{ route('Comite.show', $comite->id) }}" class="btn btn-primary btn-sm m-btn m-btn--icon-only m-btn--air"><i class="la la-eye"></i> </a>
                            <a href="{{ route('Comite.edit', $comite->id) }}" class="btn btn-info btn-sm m-btn m-btn--icon-only m-btn--air"><i class="la la-edit"></i> </a>
                            <a href="{{ route('Comite.delete', $comite->id) }}" class="btn btn-danger btn-sm m-btn m-btn--icon-only m-btn--air"><i class="la la-trash"></i> </a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script>
        (function ($) {
            $("#m_table_1").DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/French.json"
                }
            })
        })(jQuery)
    </script>
@endsection
