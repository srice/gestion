@extends('layouts.template')

@section("css")
    <link href="/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("subheader")
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Comité - {{ $comite->name }}</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
@endsection

@section('content')
    <div class="m-content">
        <div class="row">
            <div class="col-md-4">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="flaticon2-graph"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">
                                Portlet Footer
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="row align-items-center">
                            <div class="col-lg-6 m--valign-middle">
                                Portlet footer:
                            </div>
                            <div class="col-lg-6 kt-align-right">
                                <button type="submit" class="btn btn-brand">Submit</button>
                                <span class="kt-margin-left-10">or <a href="#" class="kt-link kt-font-bold">Cancel</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script>
        (function ($) {
            $("#m_table_1").DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/French.json"
                }
            })
        })(jQuery)
    </script>
@endsection
