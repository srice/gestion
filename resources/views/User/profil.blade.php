@extends('layouts.template')

@section("css")
    <link href="/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("subheader")
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Mon Profil</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
@endsection

@section('content')
    <div class="m-content">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <div class="m-portlet m-portlet--full-height  ">
                    <div class="m-portlet__body">
                        <div class="m-card-profile">
                            <div class="m-card-profile__title m--hide">
                                Your Profile
                            </div>
                            <div class="m-card-profile__pic">
                                <div class="m-card-profile__pic-wrapper">
                                    <img src="/assets/app/media/img/users/user4.jpg" alt="" />
                                </div>
                            </div>
                            <div class="m-card-profile__details">
                                <span class="m-card-profile__name">{{ auth()->user()->name }}</span>
                                <a href="" class="m-card-profile__email m-link">{{ auth()->user()->email }}</a>
                            </div>
                        </div>
                        <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                            <li class="m-nav__separator m-nav__separator--fit"></li>
                            <li class="m-nav__section m--hide">
                                <span class="m-nav__section-text">Section</span>
                            </li>
                            <li class="m-nav__item m-nav__item--active">
                                <a href="{{ route('User.profil') }}" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-profile-1"></i>
                                    <span class="m-nav__link-title">
														<span class="m-nav__link-wrap">
															<span class="m-nav__link-text">Mon Profil</span>
															<!--<span class="m-nav__link-badge"><span class="m-badge m-badge--success">2</span></span>-->
														</span>
													</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-share"></i>
                                    <span class="m-nav__link-text">Activité</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-envelope"></i>
                                    <span class="m-nav__link-text">Messages</span>
                                </a>
                            </li>
                        </ul>
                        <div class="m-portlet__body-separator"></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                        <i class="flaticon-share m--hide"></i>
                                        Mise à jour du profil
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                        Mail ({{ countUnseen() }})
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_user_profile_tab_1">
                            {{ Form::model($user, ["route" => "User.update", "class" => "m-form m-form--fit m-form--label-align-right", "id" => "formEditProfil"]) }}
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row">
                                    <div class="col-10 ml-auto">
                                        <h3 class="m-form__section">1. Détail Personnel</h3>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    {{ Form::label('name', 'Nom complet', ["class" => "col-2 col-form-label"]) }}
                                    <div class="col-7">
                                        {{ Form::text('name', $user->name, ["class" => "form-control m-input"]) }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    {{ Form::label('email', 'Adresse Mail', ["class" => "col-2 col-form-label"]) }}
                                    <div class="col-7">
                                        {{ Form::email('email', $user->email, ["class" => "form-control m-input"]) }}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    {{ Form::label('poste', 'Poste', ["class" => "col-2 col-form-label"]) }}
                                    <div class="col-7">
                                        {{ Form::email('poste', $user->poste, ["class" => "form-control m-input"]) }}
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-2">
                                        </div>
                                        <div class="col-7">
                                            <button type="submit" id="btnFormSubmit" class="btn btn-accent m-btn m-btn--air m-btn--custom">Sauvegarder</button>&nbsp;&nbsp;
                                            <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Annuler</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                        <div class="tab-pane " id="m_user_profile_tab_2">
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                                <thead>
                                    <tr>
                                        <th>Expediteur</th>
                                        <th>Sujet</th>
                                        <th>Date</th>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($inboxes as $inbox)
                                <tr style="@if(isSeen($inbox->msgno) == false) font-weight: bold; @endif">
                                    <td>{{ getSenderName($inbox->msgno) }}</td>
                                    <td>{{ getSubject($inbox->msgno) }} - <i class="text-muted">{{ substr(getBody($inbox->msgno), 0, 15) }}</i></td>
                                    <td>{{ \Carbon\Carbon::createFromTimestamp($inbox->udate)->format('d/m/Y à H:i') }}</td>
                                    <td>
                                        <a href="" class="btn btn-primary m-btn m-btn--icon-only m-btn--pill"><i class="la la-eye"></i> </a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script type="text/javascript">
        (function ($) {
            $("#m_table_1").DataTable()
            $("#formEditProfil").on('submit', function (e) {
                e.preventDefault()
                let form = $(this)
                let url = form.attr('action')
                let btn = $("#btnFormSubmit");
                let data = form.serializeArray()

                mApp.block(form)
                mApp.progress(btn)

                $.ajax({
                    url: url,
                    type: "PUT",
                    data: data,
                    statusCode: {
                        200: function (data) {
                            mApp.unblock(form)
                            mApp.unprogress(btn)
                            toastr.success("Votre profil à été mis à jour !", "Mise à jour de profil")
                        },
                        500: function (data) {
                            mApp.unblock(form)
                            mApp.unprogress(btn)
                            console.log(data);
                        }
                    }
                })
            })
        })(jQuery)
    </script>
@endsection
